# Azur Lane Fleet Builder
Welcome!

## Git LFS
This project uses Git LFS for files that are not supposed to be in source control, such as image. Make sure you have [git lfs](https://git-lfs.github.com/) installed. If you are using the Git for Windows client, make sure you enable the "LFS" checkbox during installation.

## Developing Visual Components
In order to make development of components easier and reduce the "F5-experience" you can develop components using [cosmos](https://github.com/react-cosmos/react-cosmos) this allows you to view the component and its props inside a sandbox environment, all other features you are used to such as hot-reloading still work, so you can edit your component and CSS and the component will update in your browser. The advantage is that this method is entirely state independent as you can set props in the editor in your browser or set it in the fixture.

In order to start using cosmos simple run:
```
> yarn cosmos
```
And then go to `http://localhost:8989` in your browser
