import merge = require("webpack-merge");
import common from "./webpack.common";
import MiniCSSExtractPlugin = require("mini-css-extract-plugin");

export default merge(common, {
  mode: "production",
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: [
          MiniCSSExtractPlugin.loader,
          "css-loader",
          "postcss-loader",
          "sass-loader",
        ],
      },
    ],
  },
  plugins: [
    new MiniCSSExtractPlugin({
      filename: "styles.css",
    }),
  ],
});
