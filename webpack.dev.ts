import CircularDependencyPlugin = require("circular-dependency-plugin");
import merge = require("webpack-merge");
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";

import common from "./webpack.common";

const plugins: typeof common.plugins = [
    new CircularDependencyPlugin({
        // Exclude detection of files based on a RegExp
        exclude: /a\.js|node_modules/,
        // Add errors to webpack instead of warnings
        failOnError: false,
    }),
];

if (process.env.analyzer) {
    plugins.push(new BundleAnalyzerPlugin());
}

export default merge(common, {
    mode: "development",
    devtool: "inline-source-map",
    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    devServer: {
        port: 8080,
        historyApiFallback: true,
    },
    plugins,
});
