/**
 * Any PNG file will either be converted to base64 string or replaced as url
 * so for our purposes it is always a string.
 */
declare module "*.png" {
    const value: string;
    export = value;
}
