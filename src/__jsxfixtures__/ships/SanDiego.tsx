import React = require("react");
import ShipGirlCard from "../../main/components/ShipGirlCard";
import { Rarity } from "../../main/models/ship/Rarity";

import { SanDiegoVisuals as Visuals } from "../../main/img/ships/SanDiego";
import ShipIcon from "../../main/components/ShipIcon";

const sanDiegoVisuals = Visuals.map;

const cards = (
    <div
        style={{
            display: "flex",
            flexDirection: "column",
        }}
    >
        <div>
            <ShipGirlCard
                name="San Diego"
                level={100}
                rarity={Rarity.SuperRare}
                image={sanDiegoVisuals.default.card}
            />
            <ShipGirlCard
                name="San Diego (Retrofit)"
                level={100}
                rarity={Rarity.SuperRare}
                image={sanDiegoVisuals.retrofit.card}
            />
            <ShipGirlCard
                name="Sandy Claus!"
                level={100}
                rarity={Rarity.SuperRare}
                image={sanDiegoVisuals.sandyClaus.card}
            />
        </div>
        <div>
            <ShipIcon
                level={100}
                rarity={Rarity.SuperRare}
                image={sanDiegoVisuals.default.icon}
            />
            <ShipIcon
                level={100}
                rarity={Rarity.SuperRare}
                image={sanDiegoVisuals.retrofit.icon}
            />
            <ShipIcon
                level={100}
                rarity={Rarity.SuperRare}
                image={sanDiegoVisuals.sandyClaus.icon}
            />
        </div>
    </div>
);

export default cards;
