import React = require("react");
import ShipGirlCard from "../../main/components/ShipGirlCard";
import { Rarity } from "../../main/models/ship/Rarity";

import { DownesVisuals as Visuals } from "../../main/img/ships/Downes";
import ShipIcon from "../../main/components/ShipIcon";

const downesVisuals = Visuals.map;

const cards = (
    <div
        style={{
            display: "flex",
            flexDirection: "column",
        }}
    >
        <div>
            <ShipGirlCard
                name="Downes"
                level={100}
                rarity={Rarity.Common}
                image={downesVisuals.default.card}
            />
            <ShipGirlCard
                name="ダウンズ"
                level={100}
                rarity={Rarity.Common}
                image={downesVisuals.default.card}
            />

            <ShipGirlCard
                name="Downes (Retrofit)"
                level={100}
                rarity={Rarity.Rare}
                image={downesVisuals.retrofit.card}
            />
            <ShipGirlCard
                name="ダウンズ"
                level={100}
                rarity={Rarity.Rare}
                image={downesVisuals.retrofit.card}
            />
        </div>
        <div>
            <ShipIcon
                level={100}
                rarity={Rarity.Common}
                image={downesVisuals.default.icon}
            />
            <ShipIcon
                level={100}
                rarity={Rarity.Rare}
                image={downesVisuals.retrofit.icon}
            />
        </div>
    </div>
);

export default cards;
