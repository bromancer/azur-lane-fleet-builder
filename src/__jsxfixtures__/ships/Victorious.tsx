import React = require("react");
import ShipGirlCard from "../../main/components/ShipGirlCard";
import { Rarity } from "../../main/models/ship/Rarity";

import { VictoriousVisuals as Visuals } from "../../main/img/ships/Victorious";
import ShipIcon from "../../main/components/ShipIcon";

const victoriousVisuals = Visuals.map;

const cards = (
    <div
        style={{
            display: "flex",
            flexDirection: "column",
        }}
    >
        <div>
            <ShipGirlCard
                name="Victorious"
                level={100}
                rarity={Rarity.SuperRare}
                image={victoriousVisuals.default.card}
            />
            <ShipGirlCard
                name="Victorious"
                level={100}
                rarity={Rarity.SuperRare}
                image={victoriousVisuals.casual.card}
            />
        </div>
        <div>
            <ShipIcon
                level={100}
                rarity={Rarity.SuperRare}
                image={victoriousVisuals.default.icon}
            />
            <ShipIcon
                level={100}
                rarity={Rarity.SuperRare}
                image={victoriousVisuals.casual.icon}
            />
        </div>
    </div>
);

export default cards;
