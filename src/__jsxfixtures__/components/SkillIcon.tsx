import { SkillIcon } from "../../main/components/SkillIcon";
import queensOrdersVisuals from "../../main/img/skills/QueensOrders/QueensOrders";
import React = require("react");

const icons = (
    <div>
        <SkillIcon icon={queensOrdersVisuals.default.icon} />
    </div>
);

export default icons;
