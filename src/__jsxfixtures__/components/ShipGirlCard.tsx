import React = require("react");
import ShipGirlCard from "../../main/components/ShipGirlCard";
import { Rarity } from "../../main/models/ship/Rarity";

import { IllustriousVisuals as Visuals } from "../../main/img/ships/Illustrious";

const illustriousVisuals = Visuals.map;

const cards = (
    <div>
        <ShipGirlCard
            name="イラストリアス"
            level={100}
            rarity={Rarity.SuperRare}
            image={illustriousVisuals.tea.card}
        />
        <ShipGirlCard
            name="イラストリアス"
            level={98}
            rarity={Rarity.Elite}
            image={illustriousVisuals.default.card}
        />
        <ShipGirlCard
            name="イラストリアス"
            level={80}
            rarity={Rarity.Rare}
            image={illustriousVisuals.pledge.card}
        />
        <ShipGirlCard
            name="イラストリアス"
            level={69}
            rarity={Rarity.SuperRare}
            image={illustriousVisuals.party.card}
        />
    </div>
);

export default cards;
