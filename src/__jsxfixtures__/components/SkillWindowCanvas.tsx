import SkillWindowCanvas from "../../main/components/SkillWindowCanvas";
import React = require("react");

const windows = (
    <div>
        <SkillWindowCanvas windows={[{ start: 20, duration: 8 }]} time={300} />
    </div>
);

export default windows;
