
import React = require("react");
import { ShipConfig } from "../../main/components/ShipConfig";
import { Downes } from "../../main/models/ship/Downes";
import { SkillIcon } from "../../main/components/SkillIcon";
import queensOrdersVisuals from "../../main/img/skills/QueensOrders/QueensOrders";

const config = (
    <div style={{ display: "flex", flexDirection: "row" }}>
        <ShipConfig ship={Downes.new()} />
        <div style={{ flexDirection: "column", display: "flex" }}>
            <SkillIcon icon={queensOrdersVisuals.default.icon} />
            <SkillIcon icon={queensOrdersVisuals.default.icon} />
            <SkillIcon icon={queensOrdersVisuals.default.icon} />
        </div>
    </div>
);

export default config;
