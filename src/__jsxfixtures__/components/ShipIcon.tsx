import ShipIcon, { EmptyShipIcon } from "../../main/components/ShipIcon";
import { Rarity } from "../../main/models/ship/Rarity";
import React = require("react");
import { IllustriousVisuals } from "../../main/img/ships/Illustrious";

const icons = (
    <div>
        <ShipIcon image={IllustriousVisuals.map.pledge.icon} rarity={Rarity.SuperRare} level={100} />
        <ShipIcon image={IllustriousVisuals.map.party.icon} rarity={Rarity.Elite} level={98} />
        <ShipIcon image={IllustriousVisuals.map.default.icon} rarity={Rarity.Rare} level={1} />
        <EmptyShipIcon />
        <EmptyShipIcon onClick={() => undefined} />
    </div>
);

export default icons;
