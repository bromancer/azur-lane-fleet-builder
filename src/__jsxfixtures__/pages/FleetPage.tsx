import FleetPage from "../../main/pages/Fleet";
import React = require("react");
import { ReduxMock } from "react-cosmos-redux";
import { combineReducers, createStore } from "redux";
import { fleetReducer } from "../../main/store/reducers/FleetReducer";

export default (
    <ReduxMock
        configureStore={state => createStore(
            combineReducers({ fleet: fleetReducer }),
            state,
        )}
        initialState={{ fleet: {} }}
    >
        <FleetPage />
    </ReduxMock>
);
