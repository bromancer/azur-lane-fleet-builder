import React = require("react");
import { ConnectedRouter } from "connected-react-router";
import { Switch, Route } from "react-router";
import { History } from "history";
import { Provider } from "react-redux";
import FleetPage from "./pages/Fleet";
import { createAppStore, AppStore } from "./store";

interface Props {
    history: History;
}

export class App extends React.PureComponent<Props> {
    private store: AppStore;

    public constructor(props: Props) {
        super(props);

        this.store = createAppStore(props.history);
    }

    public render() {
        /**
         * The main route is not correct, it should match / exactly
         */
        return (
            <Provider store={this.store}>
                <ConnectedRouter history={this.props.history}>
                    <Switch>
                        <Route path="/" component={FleetPage} />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        );
    }
}
