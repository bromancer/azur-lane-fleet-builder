import { GunType } from "../models/items/planes/Plane";
import { ItemTier, ItemLevel } from "../models/items/Item";
import { ShipGirl } from "../models/ship/ShipGirl";
import { SlotType } from "../models/items/slots/ItemSlot";
import FighterMath from "./FighterMath";

function mockGunShip(gun: GunType, tier: ItemTier, {
    efficiency = 1,
    count = 1,
    level = 0,
    rateOfFire = 1,
}) {
    const ship: ShipGirl = {
        itemSlots: [{
            slotType: SlotType.Plane,
            efficiency,
            count,
            item: {
                stats: { fireRate: rateOfFire },
                armaments: [{ type: "gun", name: gun }],
                level,
                tier,
            },
        }],
    } as ShipGirl;

    return ship;
}

describe("FighterMath", () => {
    describe("antiAirBurst", () => {
        it.each([
            [0, "4x20mm Cannons", 33 / 2.3],
            [10, "4x20mm Cannons", 80 / 2.3],
            [0, "4x20mm Cannons Type 99", 35 / 2.3],
            [10, "4x20mm Cannons Type 99", 85 / 2.3],
        ])("Calculates correctly for a level %d T0/3 %s gun type",
            (level: ItemLevel, gun: GunType, expected: number) => {
                const ship = mockGunShip(gun, "T0/3", { level });

                const actual = FighterMath.antiAirBurst(ship);

                expect(actual).toBeCloseTo(expected);
            });
    });
});
