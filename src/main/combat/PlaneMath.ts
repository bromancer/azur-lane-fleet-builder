import { BombWeight, GunType } from "../models/items/planes/Plane";
import { ArmorModifier } from "./CombatMath";
import { ItemLevel, ItemTier } from "../models/items/Item";
import { ArmorType } from "../models/ship/ArmorType";

interface TierValues {
    /**
     * Tier 2
     */
    T2: number;
    /**
     * Tier 0/3
     */
    TO: number;
}

interface BombData {
    baseDamage: TierValues;
    maxDamage: TierValues;
    armorModifier: ArmorModifier;
}

function bomb(
    baseT2: number,
    baseTO: number,
    maxT2: number,
    maxTO: number,
    light: number,
    medium: number,
    heavy: number,
): BombData {
    return {
        baseDamage: { T2: baseT2, TO: baseTO },
        maxDamage: { T2: maxT2, TO: maxTO },
        armorModifier: {
            light,
            medium,
            heavy,
        },
    };
}

interface GunData {
    baseDamage: TierValues;
    maxDamage: TierValues;
    baseReload: TierValues;
    maxReload: TierValues;
}

function gun(
    baseT2dmg: number,
    baseTOdmg: number,
    baseT2reload: number,
    baseTOreload: number,
    maxT2dmg: number,
    maxTOdmg: number,
    maxT2reload: number,
    maxTOreload: number,
): GunData {
    return {
        baseDamage: { T2: baseT2dmg, TO: baseTOdmg },
        maxDamage: { T2: maxT2dmg, TO: maxTOdmg },
        baseReload: { T2: baseT2reload, TO: baseTOreload },
        maxReload: { T2: maxT2reload, TO: maxTOreload },
    };
}

const GUN_TABLE = new Map<GunType, GunData>();

export default class PlaneMath {
    /**
     * Bomb damage and armor modifier
     */
    protected static readonly bombTable = new Map<BombWeight, BombData>([
        [100, bomb(63, 69, 159, 173, 0.8, 0.85, 1.0)],
        [500, bomb(132, 144, 330, 360, 0.8, 0.9, 1.1)],
        [1000, bomb(149, 161, 368, 402, 0.8, 0.95, 1.15)],
        [1600, bomb(157, 171, 393, 429, 0.75, 1.0, 1.2)],
        [2000, bomb(167, 182, 417, 456, 0.7, 1.05, 1.25)],
    ]);

    /**
     * Gun damage and reload table
     */
    protected static readonly gunTable = GUN_TABLE;

    /**
     * Armor Modifiers for torpedos
     */
    protected static readonly torpedoTable: ArmorModifier = {
        light: 0.8,
        medium: 1.1,
        heavy: 1.3,
    };

    /**
     * Calculates the damage done by a single bomb of the specified weight, tier, and level
     * against the specified armor type.
     *
     * @param weight Bomb weight classification
     * @param tier Item Tier
     * @param level Item Level
     * @param armor Target Armor
     */
    public static bombDamage(weight: BombWeight, tier: ItemTier, level: ItemLevel, armor: ArmorType): number {
        const data = this.bombTable.get(weight)!;

        const base = tier === "T2" ? data.baseDamage.T2 : data.baseDamage.TO;
        const max = tier === "T2" ? data.maxDamage.T2 : data.maxDamage.TO;

        const increment = (max - base) / 10;

        const modifier = ((a: ArmorType) => {
            switch (a) {
                case ArmorType.Heavy: return data.armorModifier.heavy;
                case ArmorType.Medium: return data.armorModifier.medium;
                case ArmorType.Light: return data.armorModifier.light;
                default: return 0;
            }
        })(armor);

        return (base + (increment * level)) * modifier;
    }

    /**
     * Calculates the damage output per second of a gun with the specified tier and level.
     *
     * @param type Gun Type
     * @param tier Gun Tier
     * @param level Gun Level
     */
    public static gunDamagePerSecond(type: GunType, tier: ItemTier, level: ItemLevel): number {
        const data = this.gunTable.get(type)!;

        const damage = this.gunDamage(type, tier, level);

        const base = tier === "T2" ? data.baseReload.T2 : data.baseReload.TO;
        const max = tier === "T2" ? data.maxReload.T2 : data.maxReload.TO;

        const increment = (max - base) / 10;

        return damage / (base + (increment * level));
    }

    /**
     * Calculates the damage output of a gun with the specified tier and level.
     *
     * @param type Gun Type
     * @param tier Gun Tier
     * @param level Gun Level
     */
    public static gunDamage(type: GunType, tier: ItemTier, level: ItemLevel): number {
        const data = this.gunTable.get(type)!;

        const base = tier === "T2" ? data.baseDamage.T2 : data.baseDamage.TO;
        const max = tier === "T2" ? data.maxDamage.T2 : data.maxDamage.TO;

        const increment = (max - base) / 10;

        return (base + (increment * level));
    }
}

// Init here so auto-complete helps me out :D
GUN_TABLE.set("4x20mm Cannons", gun(30, 33, 1.19, 1.13, 74, 80, 0.95, 0.9));
GUN_TABLE.set("4x20mm Cannons Type 99", gun(32, 35, 1.19, 1.13, 77, 85, 0.95, 0.9));
GUN_TABLE.set("2x20mm Cannos", gun(14, 15, 0.85, 0.8, 37, 40, 0.68, 0.65));
GUN_TABLE.set("6x12.7mm MG", gun(25, 27, 1.08, 1.03, 63, 69, 0.86, 0.81));
GUN_TABLE.set("4x12.7mm MG", gun(20, 22, 0.84, 0.79, 42, 46, 0.67, 0.63));
GUN_TABLE.set("2x12.7mm MG", gun(12, 14, 0.59, 0.55, 28, 32, 0.47, 0.45));
GUN_TABLE.set("8x7.7mm MG", gun(22, 26, 0.5, 0.48, 56, 61, 0.4, 0.38));
GUN_TABLE.set("4x7.7mm MG", gun(14, 16, 0.5, 0.48, 35, 38, 0.4, 0.38));
GUN_TABLE.set("2x7.7mm MG", gun(9, 10, 0.5, 0.48, 16, 18, 0.47, 0.45));
GUN_TABLE.set("1x13mm MG", gun(6, 8, 0.59, 0.48, 16, 18, 0.47, 0.45));
GUN_TABLE.set("3x7.92mm MG", gun(16, 18, 0.48, 0.46, 27, 29, 0.38, 0.36));
GUN_TABLE.set("2x7.92mm MG", gun(11, 13, 0.43, 0.40, 22, 24, 0.33, 0.30));
GUN_TABLE.set("2xMG131 13mm Cannons", gun(12, 14, 0.62, 0.59, 28, 32, 0.50, 0.48));
GUN_TABLE.set("3xMG151 20mm Cannons", gun(25, 30, 1.02, 0.97, 55, 60, 0.85, 0.82));
GUN_TABLE.set("2xMGFF 20mm Cannons", gun(17, 20, 0.64, 0.61, 38, 41, 0.52, 0.50));
