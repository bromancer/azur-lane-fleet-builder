import { ShipGirl } from "../models/ship/ShipGirl";
import { SlotType } from "../models/items/slots/ItemSlot";
import { ArmorType } from "../models/ship/ArmorType";
import { BombWeight, GunType } from "../models/items/planes/Plane";
import { ItemLevel, ItemTier } from "../models/items/Item";
import DiveBomberMath from "./DiveBomberMath";

function mockBomberShip(weight: BombWeight, tier: ItemTier, {
    efficiency = 1,
    count = 1,
    level = 0,
    bombCount = 1,
    rateOfFire = 1,
}) {
    const ship: ShipGirl = {
        itemSlots: [{
            slotType: SlotType.Plane,
            efficiency,
            count,
            item: {
                stats: { fireRate: rateOfFire },
                armaments: [{ type: "bomb", weight, count: bombCount }],
                level,
                tier,
            },
        }],
    } as ShipGirl;

    return ship;
}

function mockGunShip(gun: GunType, tier: ItemTier, {
    efficiency = 1,
    count = 1,
    level = 0,
    rateOfFire = 1,
}) {
    const ship: ShipGirl = {
        itemSlots: [{
            slotType: SlotType.Plane,
            efficiency,
            count,
            item: {
                stats: { fireRate: rateOfFire },
                armaments: [{ type: "gun", name: gun }],
                level,
                tier,
            },
        }],
    } as ShipGirl;

    return ship;
}

/**
 * Since the "per second" part is always calculated by using RoF x 2.2 + CD and we set RoF to 1 we can simply use
 * 2.3 as the value to divide by.
 */
describe("DiveBomberMath", () => {
    describe("antiShipDps", () => {
        it.each([
            [0, 100, ArmorType.Light, (69 * 0.8) / 2.3],
            [0, 100, ArmorType.Medium, (69 * 0.85) / 2.3],
            [0, 100, ArmorType.Heavy, (69 * 1.0) / 2.3],
            [10, 100, ArmorType.Light, (173 * 0.8) / 2.3],
            [10, 100, ArmorType.Medium, (173 * 0.85) / 2.3],
            [10, 100, ArmorType.Heavy, (173 * 1.0) / 2.3],
            [0, 500, ArmorType.Light, (144 * 0.8) / 2.3],
            [0, 500, ArmorType.Medium, (144 * 0.9) / 2.3],
            [0, 500, ArmorType.Heavy, (144 * 1.1) / 2.3],
            [10, 500, ArmorType.Light, (360 * 0.8) / 2.3],
            [10, 500, ArmorType.Medium, (360 * 0.9) / 2.3],
            [10, 500, ArmorType.Heavy, (360 * 1.1) / 2.3],
            [0, 1000, ArmorType.Light, (161 * 0.8) / 2.3],
            [0, 1000, ArmorType.Medium, (161 * 0.95) / 2.3],
            [0, 1000, ArmorType.Heavy, (161 * 1.15) / 2.3],
            [10, 1000, ArmorType.Light, (402 * 0.8) / 2.3],
            [10, 1000, ArmorType.Medium, (402 * 0.95) / 2.3],
            [10, 1000, ArmorType.Heavy, (402 * 1.15) / 2.3],
            [0, 1600, ArmorType.Light, (171 * 0.75) / 2.3],
            [0, 1600, ArmorType.Medium, (171 * 1.0) / 2.3],
            [0, 1600, ArmorType.Heavy, (171 * 1.2) / 2.3],
            [10, 1600, ArmorType.Light, (429 * 0.75) / 2.3],
            [10, 1600, ArmorType.Medium, (429 * 1.0) / 2.3],
            [10, 1600, ArmorType.Heavy, (429 * 1.2) / 2.3],
            [0, 2000, ArmorType.Light, (182 * 0.7) / 2.3],
            [0, 2000, ArmorType.Medium, (182 * 1.05) / 2.3],
            [0, 2000, ArmorType.Heavy, (182 * 1.25) / 2.3],
            [10, 2000, ArmorType.Light, (456 * 0.7) / 2.3],
            [10, 2000, ArmorType.Medium, (456 * 1.05) / 2.3],
            [10, 2000, ArmorType.Heavy, (456 * 1.25) / 2.3],
        ])("Calculates correctly for a level %d T0/3 1x %dlb bomb against %s armor",
            (level: ItemLevel, weight: BombWeight, armor: ArmorType, expected: number) => {
                const ship = mockBomberShip(weight, "T0/3", { level });

                const actual = DiveBomberMath.antiShipDps(ship, armor);

                expect(actual).toBeCloseTo(expected);
            });

        it.each([
            [0, 100, ArmorType.Light, 2 * (69 * 0.8) / 2.3],
            [0, 100, ArmorType.Medium, 2 * (69 * 0.85) / 2.3],
            [0, 100, ArmorType.Heavy, 2 * (69 * 1.0) / 2.3],
        ])("Calculates correct for a level %d T0/3 2x %dlb bomb against %s armor",
            (level: ItemLevel, weight: BombWeight, armor: ArmorType, expected: number) => {
                const ship = mockBomberShip(weight, "T0/3", { level, bombCount: 2 });

                const actual = DiveBomberMath.antiShipDps(ship, armor);

                expect(actual).toBeCloseTo(expected);
            });
    });

    describe("antiAirDps", () => {
        it.each([
            [0, "4x20mm Cannons", (33 / 1.13) / 2.3],
            [10, "4x20mm Cannons", (80 / 0.9) / 2.3],
            [0, "4x20mm Cannons Type 99", (35 / 1.13) / 2.3],
            [10, "4x20mm Cannons Type 99", (85 / 0.9) / 2.3],
        ])("Calculates correctly for a level %d T0/3 %s gun type",
            (level: ItemLevel, gun: GunType, expected: number) => {
                const ship = mockGunShip(gun, "T0/3", { level });

                const actual = DiveBomberMath.antiAirDps(ship);

                expect(actual).toBeCloseTo(expected);
            });
    });
});
