import { Illustrious } from "../models/ship/Illustrious";
import CombatMath from "./CombatMath";
import { ShipGirl } from "../models/ship/ShipGirl";
import HellcatT3 from "../models/items/planes/HellcatT3";
import BarracudaT3 from "../models/items/planes/BarracudaT3";

describe("CombatMath", () => {
    describe("LaunchCooldown", () => {
        it("Calculates for Illustrious", () => {
            let ship = Illustrious.new();

            ship = ShipGirl.setLevel(ship, 100);
            ship = ShipGirl.equip(ship, 0, HellcatT3.create());
            ship = ShipGirl.equip(ship, 1, HellcatT3.create());
            ship = ShipGirl.equip(ship, 2, BarracudaT3.create());

            const expected = 27.7;
            const actual = CombatMath.launchCooldown(ship);

            expect(actual).toBeCloseTo(expected, 1);
        });

        it("Calculates for Illustrious Max Planes", () => {
            let ship = Illustrious.new();

            ship = ShipGirl.setLevel(ship, 100);
            ship = ShipGirl.equip(ship, 0, HellcatT3.create(10));
            ship = ShipGirl.equip(ship, 1, HellcatT3.create(10));
            ship = ShipGirl.equip(ship, 2, BarracudaT3.create(10));

            const expected = 27.7;
            const actual = CombatMath.launchCooldown(ship);

            expect(actual).toBeLessThan(expected);
        });
    });
});
