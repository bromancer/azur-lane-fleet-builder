import { ShipGirl } from "../models/ship/ShipGirl";
import DiveBomberMath from "./DiveBomberMath";
import { isPlaneSlot } from "../models/items/slots/PlaneSlot";
import { isGun } from "../models/items/planes/Plane";

/**
 * Fighter Plane Math!
 *
 * Fighter Math is exactly the same as DiveBomberMath with the addition of
 * anti-air burst.
 */
export default class FighterMath extends DiveBomberMath {
    /**
     * Calculates ship anti-air burst damage.
     *
     * This is an implementation of the formula on the following pages:
     * - https://azurlane.koumakan.jp/List_of_Fighters
     *
     * @param ship Ship the planes are on
     *
     * @returns Damage per wave if the gun fires only once.
     */
    public static antiAirBurst(ship: ShipGirl): number {
        const planes = ship.itemSlots
            .filter(isPlaneSlot)
            .filter(s => !!s.item)
            .map(s => ({ plane: s.item!, count: s.count, efficiency: s.efficiency }));

        return planes.map(p => {
            const plane = p.plane;

            const gunDamage = plane.armaments
                .filter(isGun)
                .reduce((prev, current) => prev + this.gunDamage(
                    current.name,
                    plane.tier,
                    plane.level,
                ), 0);

            return p.efficiency * p.count * gunDamage / (plane.stats.fireRate * 2.2 + 0.1);
        }).reduce((prev, cur) => prev + cur, 0);
    }
}
