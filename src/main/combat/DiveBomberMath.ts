import PlaneMath from "./PlaneMath";
import { ShipGirl } from "../models/ship/ShipGirl";
import { ArmorType } from "../models/ship/ArmorType";
import { isPlaneSlot } from "../models/items/slots/PlaneSlot";
import { isBomb, isGun } from "../models/items/planes/Plane";

export default class DiveBomberMath extends PlaneMath {
    /**
     * Calculates the antiShipDps against an armor type. Using ship bombs.
     *
     * This is an implementation of the formula on the following pages:
     * - https://azurlane.koumakan.jp/List_of_Dive_Bombers
     * - https://azurlane.koumakan.jp/List_of_Fighters
     *
     * @param ship Ship the planes are on
     * @param targetArmor Target armor type
     *
     * @returns Damage per second.
     */
    public static antiShipDps(ship: ShipGirl, targetArmor: ArmorType): number {
        const planes = ship.itemSlots
            .filter(isPlaneSlot)
            .filter(s => !!s.item)
            .map(s => ({ plane: s.item!, count: s.count, efficiency: s.efficiency }));

        return planes.map(p => {
            const plane = p.plane;
            const bombDamage = plane.armaments
                .filter(isBomb)
                .reduce((prev, current) => prev + this.bombDamage(
                    current.weight,
                    plane.tier,
                    plane.level,
                    targetArmor,
                ) * current.count, 0);

            return (bombDamage / (plane.stats.fireRate * 2.2 + 0.1)) * p.efficiency * p.count;
        }).reduce((prev, cur) => prev + cur, 0);
    }

    /**
     * Calculates ship anti-air DPS.
     *
     * This is an implementation of the formula on the following pages:
     * - https://azurlane.koumakan.jp/List_of_Dive_Bombers
     * - https://azurlane.koumakan.jp/List_of_Fighters
     *
     * @param ship Ship the planes are on
     *
     * @returns Damage per second.
     */
    public static antiAirDps(ship: ShipGirl): number {
        const planes = ship.itemSlots
            .filter(isPlaneSlot)
            .filter(s => !!s.item)
            .map(s => ({ plane: s.item!, count: s.count, efficiency: s.efficiency }));

        return planes.map(p => {
            const plane = p.plane;

            const gunDamage = plane.armaments
                .filter(isGun)
                .reduce((prev, current) => prev + this.gunDamagePerSecond(
                    current.name,
                    plane.tier,
                    plane.level,
                ), 0);

            return (gunDamage / (plane.stats.fireRate * 2.2 + 0.1)) * p.efficiency * p.count;
        }).reduce((prev, cur) => prev + cur, 0);
    }
}
