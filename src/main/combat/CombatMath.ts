import { ShipGirl } from "../models/ship/ShipGirl";
import { WeaponSlot } from "../models/items/slots/WeaponSlot";
import { Plane } from "../models/items/planes/Plane";
import { isDefined } from "../Guards";
import { isPlaneSlot } from "../models/items/slots/PlaneSlot";

export interface ArmorModifier {
    light: number;
    medium: number;
    heavy: number;
}

export default class CombatMath {
    /**
     * Calculates the critical rate against another ship and returns the factor.
     *
     * @param attacker Attacking ship
     * @param defender Defending ship
     * @returns critical rate chance as factor (e.g. 0.12)
     */
    public static criticalRate(attacker: ShipGirl, defender: ShipGirl): number {
        /*
        Source: https://azurlane.koumakan.jp/Combat#Critical_Hits
        */

        const attackerHit = attacker.stats.accuracy;
        const attackerLuck = attacker.stats.luck;
        const defenderEvasion = defender.stats.evasion;
        const defenderLuck = defender.stats.luck;

        const levelDifference = attacker.level - defender.level;

        const criticalRate =
            // Base critical chance
            0.05 +
            (
                attackerHit /
                /*
                This 2000 is high considering accuracy is general below 200
                Eldridge has an evasion of 203 at level 100 which are all like 10% of this 2000
                value.

                I think you can get a boost of 0.1 (10%) in the best case
                */
                (attackerHit + defenderEvasion + 2000)
            ) +
            (
                /*
                Same story here, luck is usually below 100 and the defender luck is in the same
                range probably. The level difference is the deciding factor I guess which in the
                best case is a difference of 119 (lv 1 vs 120, which let's face it you don't
                need critical hits for to win).

                I think this will boost at most ~50/5000 = 0.01 (1%)
                */
                (attackerLuck - defenderLuck + levelDifference) /
                5000
            ) +
            // @TODO: Critical rate from skills/equipment
            0;

        return criticalRate;
    }

    /**
     * Calculates the launch cooldown of the Air Raid skill for aircraft carriers.
     *
     * @param ship Ship to calculate launch cooldown for
     * @returns Cooldown in seconds, or undefined if the ship has no planes.
     */
    public static launchCooldown(ship: ShipGirl): number | undefined {
        /*
        Source: https://azurlane.koumakan.jp/Combat#Reload
        */

        const planes: Plane[] = ship.itemSlots
            // We only want planes
            .filter(isPlaneSlot)
            .map(slot => slot.item)
            // Make sure we get equipped items only
            .filter(isDefined);

        if (planes.length <= 0) {
            // No planes, no service. Also we can't divide by zero down the line
            return undefined;
        }

        // Sum of the plane cooldown times the number of planes
        const planesAndCooldown = planes
            .reduce((acc, plane) => acc + (plane.stats.fireRate * plane.stats.count), 0);
        // Sum of the number of planes
        const numberOfPlanes = planes
            .reduce((acc, plane) => acc + plane.stats.count, 0);

        // @TODO: Calculate skill bonus
        const skillBonus = 0;

        // Calculation of stat/skill based reload enhancement
        const reloadFactor = Math.sqrt(200 / (100 + ship.stats.reload * (1 + skillBonus)));

        // Formula has 2 magic empirical factors. It makes you wonder how official this wiki page
        // is if they need to determine these factors empirically instead of just looking into
        // their own source code.
        const result = (planesAndCooldown / numberOfPlanes) * 2.18 * reloadFactor + 0.3;

        // Returning NaN is also valid, but it's so annoying.
        return isNaN(result) ? undefined : result;
    }

    public static gunDamage(ship: ShipGirl, slot: WeaponSlot): number {
        if (!slot.item) { return 0; }

        const totalFirepower = ship.stats.firepower + ship.statsBoost.firepower;

        // @TODO: Get buff from ammo and skills
        const ammoAndSkillBuff = 1;
        // @TODO: Enemy debuff?
        // @TODO: Find hunter skills

        const powerFactor = (100 + totalFirepower * (1 + 0 + 0)) / 100;

        const item = slot.item;

        return item.stats.damage
            * item.coefficient
            * slot.efficiency
            * 1 // ArmorBonus
            * 1 // LevelAdvantage
            * ammoAndSkillBuff
            * 1 // Enemy Debuff
            * powerFactor
            + Math.floor((Math.random() * 5) - 1);
    }
}
