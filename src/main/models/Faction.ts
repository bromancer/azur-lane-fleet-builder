export enum Faction {
    EagleUnion = "Eagle Union",
    SakuraEmpire = "Sakura Empire",
    RoyalNavy = "Royal Navy",
    Ironblood = "Ironblood",
    EasternRadiance = "Eastern Radiance",
    NorthUnion = "North Union",
    IrisLibre = "Iris Libre",
    VichyaDominion = "Vichya Dominion",
    Neptunia = "Neptunia",
}
