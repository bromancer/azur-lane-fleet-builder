import { Enhancement } from "./enhancements/Enhancement";
import { ShipInfo } from "../ship/ShipInfo";
import { SkillRequirement } from "./SkillRequirement";

interface ISkillWindow {
    readonly type: SkillActivationType;
    readonly skill: ISkill;
}

export type IncreaseType = "percentage" | "incremental";
export type SkillLevel = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export interface PermanentSkillWindow extends ISkillWindow {
    readonly type: SkillActivationType.Permanent;
}

export interface OneShotSkillWindow extends ISkillWindow {
    readonly type: SkillActivationType.OneShot;
    /**
     * Number of seconds since start of battle this skill activates
     */
    readonly trigger: number;
    /**
     * Activation chance, number ranges from 0.0 to 1.0
     */
    readonly chance: number;
}

export interface TimedSkillWindow extends ISkillWindow {
    readonly type: SkillActivationType.Timed;
    /**
     * Number of seconds since start of battle this window activates
     */
    readonly start: number;
    /**
     * Number of seconds since start of battle this window closes
     */
    readonly end: number;
    /**
     * Activation chance, number ranges from 0.0 to 1.0
     */
    readonly chance: number;
}

export interface ImpactSkillWindow extends ISkillWindow {
    readonly type: SkillActivationType.Impact;
}

export type SkillWindow = PermanentSkillWindow | OneShotSkillWindow | TimedSkillWindow | ImpactSkillWindow;

export interface ISkillRequirementMatch {
    requirement: SkillRequirement;
    matches: boolean;
}

export interface ISkill {
    readonly id: string;
    // readonly Type: SkillType;
    readonly activationType: SkillActivationType;

    matchesRequirements(info: ShipInfo): ISkillRequirementMatch[];
    /**
     * Calculates activation windows based on the ship girl provided. The ship girl
     * is used in order to determine weapon speed etc...
     *
     * @param info Info about the ship (her level etc)
     * @param end Number of seconds until battle end
     */
    calculate(info: ShipInfo, end: number): SkillWindow[];
    /**
     * Applies the effects of this skill to the ship girl. If the ship girl is eligible
     * the stats will be applied as if the skill were active.
     *
     * @param info Info about the ship (her level etc)
     * @param target Target this skill is getting applied to
     *
     * @returns Ship girl object with enhanced stats
     */
    applyTo(info: ShipInfo, target: SkillTarget): Enhancement[];
}

export enum SkillType {
    Support = "support",
    Offensive = "offensive",
    Defensive = "defensive",
}

export enum SkillActivationType {
    /**
     * Skill is active for as long as the ship is alive
     */
    Permanent = "permanent",
    /**
     * Skill is triggered by main gun fire
     */
    OneShot = "one_shot",
    /**
     * Skill is triggered after a set amount of time
     */
    Timed = "timed",
    /**
     * Skill is triggered by impact with an enemy
     */
    Impact = "impact",
}

export enum SkillTarget {
    None = "none",
    Self = "self",
    Ally = "ally",
    Enemy = "enemy",
}

export enum SkillTargetType {
    None = "none",
    Vanguard = "vanguard",
    Main = "main",
    Destroyer = "destroyer",
    LightCruiser = "light_cruiser",
    HeavyCruiser = "heavy_cruiser",
    BattleCruiser = "battlecruiser",
    Carrier = "carrier",
    Fleet = "fleet",
}
