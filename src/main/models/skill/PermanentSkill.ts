import {
    ISkill,
    SkillActivationType,
    SkillWindow,
    SkillTarget,
    SkillTargetType,
    SkillLevel,
    ISkillRequirementMatch,
} from "./Skill";
import { ShipEnhancement } from "./enhancements/ShipEnhancement";

import { Faction } from "../Faction";
import { StatEnhancement } from "./enhancements/StatEnhancement";
import { Enhancement } from "./enhancements/Enhancement";
import { ShipInfo } from "../ship/ShipInfo";

export abstract class PermanentSkill implements ISkill {
    public readonly activationType: SkillActivationType = SkillActivationType.Permanent;

    public abstract get id(): string;
    protected abstract get skillTargetType(): SkillTargetType;
    protected abstract get skillTarget(): SkillTarget;
    protected abstract get faction(): Faction | undefined;
    protected abstract get enhancements(): ShipEnhancement[];

    public calculate(_info: ShipInfo, _end: number): SkillWindow[] {
        return [{ type: SkillActivationType.Permanent, skill: this }];
    }

    public abstract applyTo(info: ShipInfo, target: SkillTarget): Enhancement[];
    public abstract matchesRequirements(info: ShipInfo): ISkillRequirementMatch[];
}

// tslint:disable-next-line:max-classes-per-file
export class QueensOrders extends PermanentSkill {
    protected skillTargetType: SkillTargetType = SkillTargetType.Fleet;
    protected skillTarget: SkillTarget = SkillTarget.Ally;
    protected faction: Faction | undefined = Faction.RoyalNavy;
    public readonly id = "queens_orders";

    protected readonly enhancements: ShipEnhancement[];

    public constructor(level: SkillLevel) {
        super();

        this.enhancements = [
            new StatEnhancement(this, "firepower", 5 + (level * 1), "percentage"),
            new StatEnhancement(this, "torpedo", 5 + (level * 1), "percentage"),
            new StatEnhancement(this, "aviation", 5 + (level * 1), "percentage"),
            new StatEnhancement(this, "antiAir", 5 + (level * 1), "percentage"),
            new StatEnhancement(this, "reload", 5 + (level * 1), "percentage"),
            new StatEnhancement(this, "evasion", 5 + (level * 1), "percentage"),
        ];
    }

    public matchesRequirements(_info: ShipInfo): ISkillRequirementMatch[] {
        return [];
    }

    public applyTo(info: ShipInfo, _target: SkillTarget): Enhancement[] {
        if (info.self.faction !== this.faction) {
            return [];
        }

        return [];
    }
}
