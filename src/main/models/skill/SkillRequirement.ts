import { Faction } from "../Faction";
import { HullType } from "../ship/HullType";
import { LimitBreak } from "../ship/ShipGirl";

interface IAllyComposition {
    type: "ally";
    id: number;
}

interface IClassComposition {
    type: "class";
    class: string;
    minimum?: number;
}

interface IFactionComposition {
    type: "faction";
    faction: Faction;
    minimum?: number;
}

interface IHullComposition {
    type: "hull";
    hull: HullType;
    minimum?: number;
}

export type Composition = IAllyComposition
    | IClassComposition
    | IFactionComposition
    | IHullComposition;

interface ICompositionSkillRequirement {
    type: "composition";
    composition: Composition;
}

interface IFlagshipSkillRequirement {
    type: "flagship";
}

interface ILimitBreakSkillRequirement {
    type: "limitbreak";
    level: LimitBreak;
}

export type SkillRequirement = ICompositionSkillRequirement
    | IFlagshipSkillRequirement
    | ILimitBreakSkillRequirement;
