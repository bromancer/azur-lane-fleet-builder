import {
    ISkill,
    SkillType,
    SkillActivationType,
    SkillTarget,
    IncreaseType,
    SkillWindow,
    ISkillRequirementMatch,
    TimedSkillWindow,
    SkillLevel,
} from "./Skill";
import { ShipStat } from "../ship/ShipStats";
import { Enhancement } from "./enhancements/Enhancement";
import {
    ShipInfo,
    IFleetInfo,
} from "../ship/ShipInfo";
import {
    SkillRequirement,
    Composition,
} from "./SkillRequirement";
import { Faction } from "../Faction";
import { HullType } from "../ship/HullType";
import { ShipGirl } from "../ship/ShipGirl";

interface IEnhancementFile {
    type: "stat" | "damage";
    target: SkillTarget;
}

interface StatEnhancementFile extends IEnhancementFile {
    type: "stat";
    stat: ShipStat;
    increase: {
        type: IncreaseType;
        increase: number | number[];
    };
}

type EnhancementFile = StatEnhancementFile;

interface TimeInfo {
    step: number;
    end: number;
}

interface ISkillFile {
    id: string;
    type: SkillType;
    activationType: SkillActivationType;
    enhancements: EnhancementFile[];
    chance?: number | number[];
    requirements?: SkillRequirement[];
}

interface IStartTime {
    initial: number | number[];
    start: number | number[];
}

function isNumber(x: unknown): x is number {
    return typeof x === "number";
}

function isNumberArray(x: unknown): x is number[] {
    if (!Array.isArray(x)) {
        return false;
    }

    for (const n of x) {
        if (!isNumber(n)) {
            return false;
        }
    }

    return true;
}

function isNumberOrNumberArray(x: unknown): x is number | number[] {
    return isNumber(x) || isNumberArray(x);
}

function isStartTime(x: unknown): x is IStartTime {
    if (typeof x !== "object") {
        return false;
    }

    const obj = x as any;

    return obj.hasOwnProperty("initial")
        && isNumberOrNumberArray(obj.initial)
        && obj.hasOwnProperty("start")
        && isNumberOrNumberArray(obj.start);
}

interface TimedSkillFile extends ISkillFile {
    start: number | number[] | IStartTime;
    duration: number | number[];
    activationType: SkillActivationType.Timed;
}

type SkillFile = TimedSkillFile;

type ShipStatSelector<T> = (ship: ShipGirl) => T;

abstract class Skill implements ISkill {
    public readonly id: string;
    public readonly activationType: SkillActivationType;

    private readonly requirements?: SkillRequirement[];
    protected readonly enhancements?: any[];
    private readonly chance?: number | number[];

    protected constructor(file: ISkillFile) {
        this.id = file.id;
        this.activationType = file.activationType;
        this.requirements = file.requirements;

        // TODO: Map to enhancement
        this.enhancements = file.enhancements;
        this.chance = file.chance;
    }

    protected getChance(level: SkillLevel): number {
        if (this.chance === undefined) {
            return 1.0;
        }

        if (isNumber(this.chance)) {
            return this.chance;
        } else {
            return this.chance[level - 1] || 0;
        }
    }

    private hasAllyInFleet(self: ShipGirl, fleet: IFleetInfo, id: number): boolean {
        if (self.id === id) {
            // Can't count self
            return false;
        }

        const ships = this.fleetToShips(fleet);

        for (const ship of ships) {
            if (ship.id === id) {
                return true;
            }
        }

        return false;
    }

    private fleetToShips(fleet: IFleetInfo): ShipGirl[] {
        const ships = [fleet.flagship];
        ships.concat(fleet.main);
        ships.concat(fleet.vanguard);

        return ships;
    }

    private hasMatchesInFleet<T>(
        fleet: IFleetInfo,
        property: T,
        selector: ShipStatSelector<T>,
        minimum?: number): boolean {
        const ships = this.fleetToShips(fleet);

        const min = minimum || 1;
        let current = 0;

        for (const ship of ships) {
            const prop = selector(ship);

            current += prop === property ? 1 : 0;
        }

        return min <= current;
    }

    private hasClassInFleet(fleet: IFleetInfo, cls: string, minimum?: number): boolean {
        return this.hasMatchesInFleet<typeof cls>(fleet, cls, (ship) => ship.shipClass, minimum);
    }

    private hasFactionInFleet(fleet: IFleetInfo, faction: Faction, minimum?: number): boolean {
        return this.hasMatchesInFleet<typeof faction>(fleet, faction, (ship) => ship.faction, minimum);
    }

    private hasHullInFleet(fleet: IFleetInfo, hull: HullType, minimum?: number): boolean {
        return this.hasMatchesInFleet<typeof hull>(fleet, hull, (ship) => ship.hull, minimum);
    }

    private isFlagship(ship: ShipGirl, fleet: IFleetInfo): boolean {
        return ship.id === fleet.flagship.id;
    }

    private isCompositionMatch(comp: Composition, info: ShipInfo): boolean {
        switch (comp.type) {
            case "ally":
                return this.hasAllyInFleet(info.self, info.fleet, comp.id);
            case "class":
                return this.hasClassInFleet(info.fleet, comp.type, comp.minimum);
            case "faction":
                return this.hasFactionInFleet(info.fleet, comp.faction, comp.minimum);
            case "hull":
                return this.hasHullInFleet(info.fleet, comp.hull, comp.minimum);
            default:
                return false;
        }
    }

    public matchesRequirements(info: ShipInfo): ISkillRequirementMatch[] {
        const matches: ISkillRequirementMatch[] = [];

        if (!this.requirements || this.requirements.length === 0) {
            return matches;
        }

        for (const req of this.requirements) {
            let isMatch: boolean;

            switch (req.type) {
                case "composition":
                    isMatch = this.isCompositionMatch(req.composition, info);
                    break;
                case "flagship":
                    isMatch = this.isFlagship(info.self, info.fleet);
                    break;
                case "limitbreak":
                    isMatch = info.self.limitBreak === req.level;
                    break;
                default:
                    isMatch = false;
                    break;
            }

            matches.push({ requirement: req, matches: isMatch });
        }

        return matches;
    }

    protected matchesAllRequirements(info: ShipInfo): boolean {
        return !this.matchesRequirements(info).some(r => !r.matches);
    }

    public abstract calculate(info: ShipInfo, end: number): SkillWindow[];
    public abstract applyTo(info: ShipInfo, target: SkillTarget): Enhancement[];
}

// tslint:disable-next-line:max-classes-per-file
class TimedSkillImpl extends Skill {
    protected readonly start: number | number[] | IStartTime;
    protected duration: number | number[];

    public constructor(file: TimedSkillFile) {
        super(file);

        this.start = file.start;
        this.duration = file.duration;
    }

    private getDuration(level: SkillLevel): number {
        if (isNumber(this.duration)) {
            return this.duration;
        } else {
            return this.duration[level - 1] || 0;
        }
    }

    private getStart(level: SkillLevel, start: number | number[]): number {
        if (isNumber(start)) {
            return start;
        } else {
            return start[level - 1] || 300;
        }
    }

    private constructWindow(
        start: number,
        duration: number,
        chance: number,
        timeInfo: { step: number, end: number }): TimedSkillWindow {
        timeInfo.step = timeInfo.step + start;
        timeInfo.end = timeInfo.step + duration;

        return {
            start: timeInfo.step,
            end: timeInfo.end,
            chance,
            skill: this,
            type: SkillActivationType.Timed,
        };
    }

    private constructWindows(
        windows: SkillWindow[],
        info: TimeInfo,
        start: number,
        duration: number,
        chance: number,
        end: number) {
        while (info.step + start < end) {
            windows.push(this.constructWindow(
                start,
                duration,
                chance,
                info));
        }
    }

    public calculate(info: ShipInfo, end: number): SkillWindow[] {
        if (!this.matchesAllRequirements(info)) {
            return [];
        }

        const windows: SkillWindow[] = [];

        const level = info.skillLevel[this.id];
        const chance = this.getChance(level);
        const duration = this.getDuration(level);
        const timeInfo: TimeInfo = {
            step: 0,
            end: 0,
        };

        if (isNumberOrNumberArray(this.start)) {
            this.constructWindows(
                windows,
                timeInfo,
                this.getStart(level, this.start),
                duration,
                chance,
                end);
        } else if (isStartTime(this.start)) {
            windows.push(this.constructWindow(
                this.getStart(level, this.start.initial),
                duration,
                chance,
                timeInfo));

            this.constructWindows(
                windows,
                timeInfo,
                this.getStart(level, this.start.start),
                duration,
                chance,
                end);
        }

        return windows;
    }

    public applyTo(_info: ShipInfo, _target: SkillTarget): Enhancement[] {
        throw new Error("Method not implemented.");
    }
}

export default function parseSkillFile(file: any): ISkill | null {
    const f = file as SkillFile;

    if (f.activationType === SkillActivationType.Timed) {
        return new TimedSkillImpl(f);
    }

    return null;
}
