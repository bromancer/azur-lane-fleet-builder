import { DamageEnhancement } from "./DamageEnhancement";
import { StatEnhancement } from "./StatEnhancement";

export type ShipEnhancement = StatEnhancement | DamageEnhancement;
