import { Enhancement } from "./Enhancement";
import { SkillTargetType, IncreaseType } from "../Skill";

export interface DamageEnhancement extends Enhancement {
    readonly target: SkillTargetType;
    readonly type: IncreaseType;
    readonly damageIncrease: number;
}
