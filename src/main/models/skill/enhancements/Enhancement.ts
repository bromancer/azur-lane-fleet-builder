import { ISkill } from "../Skill";
import { StatEnhancement } from "./StatEnhancement";

export interface Enhancement {
    readonly appliedBy: ISkill;
    compareTo: (other: StatEnhancement) => number;
    conflictsWith: (other: StatEnhancement) => boolean;
}
