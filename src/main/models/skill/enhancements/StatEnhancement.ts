import { Enhancement } from "./Enhancement";
import { IncreaseType, ISkill } from "../Skill";
import { ShipStat } from "../../ship/ShipStats";

export class StatEnhancement implements Enhancement {
    public appliedBy: ISkill = this.applier;

    public constructor(
        private applier: ISkill,
        protected readonly stat: ShipStat,
        protected readonly increase: number | number[],
        protected readonly type: IncreaseType) {
    }

    public compareTo(_other: StatEnhancement): number {
        return 0;
    }

    public conflictsWith(_other: StatEnhancement): boolean {
        return false;
    }
}
