import { Equipment, ItemClassification } from "../Item";

export interface Auxiliary extends Equipment {
    readonly classification: ItemClassification.Auxiliary;
}
