import { Plane, planeStatsUpdater, PlaneStats } from "./Plane";
import { ItemClassification, ItemLevel, Equipment } from "../Item";
import { ItemType } from "../ItemType";
import { SanDiegoVisuals } from "../../../img/ships/SanDiego";

export default class BarracudaT3 {
    private constructor() { }

    public static readonly id: number = 2;
    public static create(level: ItemLevel = 0): Plane {
        return {
            classification: ItemClassification.Plane,
            excluded: [],
            id: this.id,
            level,
            stats: {
                aviation: 45,
                count: 3,
                damage: Equipment.autoValue(108, 288, level),
                fireRate: Equipment.autoValue(12.64, 10.31, level),
                planeHealth: Equipment.autoValue(35, 260, level),
            },
            skill: undefined,
            type: ItemType.TorpedoPlanes,
            armaments: [
                { type: "gun", name: "2x7.7mm MG" },
                { type: "torpedo", mode: "common", count: 3 },
            ],
            tier: "T0/3",
            visuals: SanDiegoVisuals.map.default,
            name: "Fairey Barracuda",
        };
    }

    @planeStatsUpdater()
    public static stats(level: ItemLevel): PlaneStats {
        return {
            aviation: 45,
            count: 3,
            damage: Equipment.autoValue(108, 288, level),
            fireRate: Equipment.autoValue(12.64, 10.31, level),
            planeHealth: Equipment.autoValue(35, 260, level),
        };
    }
}
