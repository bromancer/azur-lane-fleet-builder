import { ItemClassification, Equipment, ItemLevel } from "../Item";
import { ItemType } from "../ItemType";
import { Plane, PlaneStats, planeStatsUpdater } from "./Plane";
import { SanDiegoVisuals } from "../../../img/ships/SanDiego";

export default class HellcatT3 {
    private constructor() { }

    private static createStats(level: ItemLevel): PlaneStats {
        return {
            aviation: 12,
            count: 3,
            damage: Equipment.autoValue(108, 288, level),
            fireRate: Equipment.autoValue(12.64, 10.31, level),
            planeHealth: Equipment.autoValue(35, 260, level),
        };
    }

    public static readonly id: number = 2;
    public static create(level: ItemLevel = 0): Plane {
        return {
            classification: ItemClassification.Plane,
            excluded: [],
            id: this.id,
            level,
            stats: this.createStats(level),
            skill: undefined,
            type: ItemType.FighterPlanes,
            tier: "T0/3",
            armaments: [
                { type: "gun", name: "4x12.7mm MG" },
                { type: "bomb", weight: 500, count: 2 },
                { type: "gun", name: "2x20mm Cannos" },
            ],
            visuals: SanDiegoVisuals.map.default,
            name: "F6F Hellcat",
        };
    }

    @planeStatsUpdater()
    public static stats(level: ItemLevel): PlaneStats {
        return this.createStats(level);
    }
}
