import { ItemClassification, Equipment, ItemLevel, Item } from "../Item";
import update from "immutability-helper";

type PlaneStat = keyof PlaneStats;

export function isPlaneStat(x: string): x is PlaneStat {
    switch (x) {
        case "fireRate":
        case "damage":
        case "health":
        case "count":
        case "aviation":
            return true;
        default:
            return false;
    }
}

export interface PlaneStats {
    /**
     * Plane cooldown or Rate of Fire in seconds;
     */
    readonly fireRate: number;
    readonly damage: number;
    readonly planeHealth: number;
    /**
     * Number of planes, this is usually increased by retrofit
     */
    readonly count: number;
    readonly aviation: number;
}

export type BombWeight = 100 | 500 | 1000 | 1600 | 2000;

interface Bomb {
    weight: BombWeight;
    type: "bomb";
    count: number;
}

export type GunType = "4x20mm Cannons"
    | "4x20mm Cannons Type 99"
    | "2x20mm Cannos"
    | "6x12.7mm MG"
    | "4x12.7mm MG"
    | "2x12.7mm MG"
    | "2x7.7mm MG"
    | "8x7.7mm MG"
    | "4x7.7mm MG"
    | "2x7.7mm MG"
    | "1x13mm MG"
    | "3x7.92mm MG"
    | "2x7.92mm MG"
    | "2xMG131 13mm Cannons"
    | "3xMG151 20mm Cannons"
    | "2xMGFF 20mm Cannons";

interface Gun {
    type: "gun";
    name: GunType;
}

interface Torpedo {
    type: "torpedo";
    mode: "common" | "sakura";
    count: number;
}

export type Armament = Bomb | Gun | Torpedo;

export function isBomb(x: Armament): x is Bomb {
    return x.type === "bomb";
}

export function isGun(x: Armament): x is Gun {
    return x.type === "gun";
}

export function isTorpedo(x: Armament): x is Torpedo {
    return x.type === "torpedo";
}

export interface Plane extends Equipment {
    readonly classification: ItemClassification.Plane;
    readonly stats: PlaneStats;
    readonly armaments: Armament[];
}

export class Plane {
    private constructor() { }

    public static setLevel(plane: Plane, level: ItemLevel): Plane {
        const newStats = planeUpdaters.get(plane.id);

        return update(plane, {
            stats: { $set: newStats(level) },
        });
    }
}

export function isPlane(x: Item): x is Plane {
    return x.classification === ItemClassification.Plane;
}

type StatsFunc = (level: ItemLevel) => PlaneStats;

interface StatUpdateProvider {
    id: number;
    stats: StatsFunc;
}

const defaultUpdater: StatsFunc = () => ({
    aviation: 0,
    count: 0,
    damage: 0,
    fireRate: 0,
    planeHealth: 0,
});

const _updaters = new Map<number, StatsFunc>();

export function planeStatsUpdater() {
    return (target: StatUpdateProvider, _propertyKey: string, _descriptor: PropertyDescriptor) => {
        _updaters.set(target.id, target.stats);
    };
}

const planeUpdaters = {
    get: (id: number): StatsFunc => {
        return _updaters.get(id) || defaultUpdater;
    },
};
