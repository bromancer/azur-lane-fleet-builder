import { Plane } from "../planes/Plane";
import { SlotType, Slot, ItemLimitBreaks } from "./ItemSlot";
import update from "immutability-helper";
import { LimitBreak } from "../../ship/ShipGirl";
import { ItemType } from "../ItemType";

export interface PlaneUpgrade {
    readonly efficiency: number;
    readonly count: number;
}

export type PlaneLimitBreaks = ItemLimitBreaks<PlaneUpgrade>;

export interface PlaneSlot extends Slot<Plane> {
    readonly slotType: SlotType.Plane;
    readonly efficiency: number;
    /**
     * Number of planes in the slot.
     */
    readonly count: number;

    readonly limitBreaks: PlaneLimitBreaks;
}

export class PlaneSlot {
    protected constructor() { }

    public static limitBreak(slot: PlaneSlot, limitBreak: LimitBreak): PlaneSlot {
        const values = slot.limitBreaks.get(limitBreak) || { efficiency: 1.0, count: 1 };

        return update(slot, {
            efficiency: { $set: values.efficiency },
            count: { $set: values.count },
        });
    }

    protected static _new(type: ItemType, limitBreaks: PlaneLimitBreaks, excluded: number[]): PlaneSlot {
        const values = limitBreaks.get(0) || { efficiency: 1.0, count: 1 };

        return {
            ...values,
            excluded,
            itemTypes: [type],
            slotType: SlotType.Plane,
            limitBreaks,
        };
    }
}

export function isPlaneSlot(x: Slot): x is PlaneSlot {
    return x.slotType === SlotType.Plane;
}
