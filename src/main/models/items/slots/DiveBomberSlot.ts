import { ItemType } from "../ItemType";
import { ItemLimitBreaks } from "./ItemSlot";
import { PlaneSlot, PlaneUpgrade } from "./PlaneSlot";

export class DiveBomberSlot extends PlaneSlot {
    private constructor() { super(); }

    public static new(limitBreaks: ItemLimitBreaks<PlaneUpgrade>, excluded: number[] = []): PlaneSlot {
        return this._new(ItemType.DiveBomberPlanes, limitBreaks, excluded);
    }
}
