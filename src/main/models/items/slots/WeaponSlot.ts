import { Weapon } from "../weapons/Weapon";
import { Slot, SlotType, ItemLimitBreaks } from "./ItemSlot";

export interface WeaponUpgrade {
    readonly efficiency: number;
    readonly count: number;
}

export type WeaponLimitBreaks = ItemLimitBreaks<WeaponUpgrade>;

export interface WeaponSlot extends Slot<Weapon> {
    efficiency: number;
    slotType: SlotType.Weapon;
    count: number;
    limitBreaks: WeaponLimitBreaks;
}

export function isWeaponSlot(x: Slot): x is WeaponSlot {
    return x.slotType === SlotType.Weapon;
}
