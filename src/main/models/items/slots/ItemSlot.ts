import { ItemType } from "../ItemType";
import { PlaneSlot, isPlaneSlot } from "./PlaneSlot";
import { WeaponSlot, isWeaponSlot } from "./WeaponSlot";
import { Item } from "../Item";
import update from "immutability-helper";
import { LimitBreak } from "../../ship/ShipGirl";
import { isPlane } from "../planes/Plane";
import { isWeapon } from "../weapons/Weapon";
import { Auxiliary } from "../auxiliary/Auxiliary";

export enum SlotType {
    Plane = "Plane",
    Weapon = "Weapon",
    Auxiliary = "Auxiliary",
}

export interface Slot<T = any> {
    readonly itemTypes: ItemType[];
    /**
     * List of equipment ids that are not allowed to mount this slot.
     *
     * Only use this to exclude items that are now allowed to be equipped
     * even though their type matches.
     *
     * Example of this is the Fairy Barracuda that is a Torpedo Bomber, however
     * Graf Zepplin cannot equip it, even though she has a Torpedo Bomber slot.
     */
    readonly excluded: number[];
    readonly item?: T;
    readonly slotType: SlotType;
}

export type ItemLimitBreaks<T> = Map<LimitBreak, T>;

export class Slot {
    private constructor() { }

    /**
     * Equips or unequips an item in a slot. Do not use directly! Should only
     * be used by the ShipGirl equip function as a helper.
     *
     * @param slot Slot to equip item in.
     * @param item Item to equip.
     */
    public static equip(slot: ItemSlot, item?: Item): ItemSlot {
        // For some reason you can't put this all together in an or because
        // typescript will think it's a Weapon | Plane | undefined which can't possibly fit
        // on a (Plane | undefined) | (Weapon | undefined)
        if (!item) {
            return update(slot, { item: { $set: item } });
        }

        if (isPlaneSlot(slot) && isPlane(item)) {
            return update(slot, { item: { $set: item } });
        }

        if (isWeaponSlot(slot) && isWeapon(item)) {
            return update(slot, { item: { $set: item } });
        }

        return slot;
    }

    /**
     * Applies limit break upgrades to the slot. Do not use directly! Should only
     * be used by the ShipGirl equip function as a helper.
     *
     * @param slot Slot to upgrade
     * @param limitBreak LimitBreak level
     */
    public static limitBreak(slot: ItemSlot, limitBreak: LimitBreak): ItemSlot {
        switch (slot.slotType) {
            case SlotType.Plane:
                return PlaneSlot.limitBreak(slot, limitBreak);
            default:
                return slot;
        }
    }
}

export interface AuxiliarySlot extends Slot<Auxiliary> {
    slotType: SlotType.Auxiliary;
}

export type ItemSlot = WeaponSlot | PlaneSlot | AuxiliarySlot;
