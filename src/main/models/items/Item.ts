import { ItemType } from "./ItemType";
import { ShipStats } from "../ship/ShipStats";
import { Weapon } from "./weapons/Weapon";
import { Plane } from "./planes/Plane";
import { IEquipmentVisuals } from "../../img/Img";

export type ItemLevel = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;
export enum ItemClassification {
    Weapon,
    Plane,
    Auxiliary,
}
export type ItemTier = "T2" | "T0/3";

export interface Equipment {
    readonly id: number;
    readonly type: ItemType;
    readonly classification: ItemClassification;
    readonly tier: ItemTier;
    /**
     * List of ship ids that are not allowed to use this item.
     */
    readonly excluded: number[];
    readonly level: ItemLevel;
    // @TODO: Some items have skills
    readonly skill: any;
    // Not all stats are mandatory
    readonly stats: Partial<ShipStats>;
    readonly visuals: IEquipmentVisuals;
    readonly name: string;
}

export type Item = Weapon | Plane;

export class Equipment {
    private constructor() { }

    public static setLevel(item: Plane, level: ItemLevel): Plane;
    public static setLevel(item: Weapon, level: ItemLevel): Weapon;
    public static setLevel(item: Item, level: ItemLevel): Item {
        switch (item.classification) {
            case ItemClassification.Weapon:
                return Weapon.setLevel(item, level);
            case ItemClassification.Plane:
                return Plane.setLevel(item, level);
            default:
                return item;
        }
    }

    /**
     * Calculates a value of the value based on item level.
     *
     * @param base Value at item level 0.
     * @param max Value at item level 10.
     * @param level Level of the item
     *
     * @returns Estimated value.
     */
    public static autoValue(base: number, max: number, level: ItemLevel): number {
        switch (level) {
            case 0: return base;
            case 10: return max;
            default:
                return Math.round(base + (((max - base) / 10) * level));
        }
    }
}
