import { Equipment, ItemLevel, ItemClassification, Item } from "../Item";
import { ItemType } from "../ItemType";
import { AmmoType } from "./AmmoType";
import update from "immutability-helper";

export type WeaponStat = keyof WeaponStats;

export function isWeaponStat(x: string): x is WeaponStat {
    switch (x) {
        case "fireRate":
        case "damage":
            return true;
        default:
            return false;
    }
}

/**
 * Container for weapon stats that scale with level.
 */
export interface WeaponStats {
    /**
     * Cool down rate of the gun.
     */
    fireRate: number;
    /**
     * Damage of a single shot in a volley.
     *
     * Example: DMG 101 x4, means damage of 101.
     */
    damage: number;
    firepower: number;
    antiAir: number;
}

export interface Weapon extends Equipment {
    readonly type: ItemType;
    readonly classification: ItemClassification.Weapon;
    /**
     * Number of shots fired in a single volley.
     *
     * Example: DMG 101 x 4, means a multiplier of 4.
     */
    readonly multiplier: number;

    readonly stats: WeaponStats;
    /**
     * Degree in front of the ship this gun can fire.
     */
    readonly firingAngle: number;
    /**
     * Frontal cone angle at which the projectiles leave the gun.
     */
    readonly spreadRange: number;
    /**
     * Maximum range of the gun.
     */
    readonly range: number;
    /**
     * Gun's ammo type.
     */
    readonly ammo: AmmoType;
    /**
     * Coefficiency factor of the gun. The wiki has this as a % so simply do /100.
     */
    readonly coefficient: number;
}

export class Weapon {
    private constructor() { }

    public static setLevel(weapon: Weapon, level: ItemLevel): Weapon {
        const newStats = weaponUpdaters.get(weapon.id);

        return update(weapon, {
            stats: { $set: newStats(level) },
        });
    }
}

export function isWeapon(x: Item): x is Weapon {
    return x.classification === ItemClassification.Weapon;
}

type StatsFunc = (level: ItemLevel) => WeaponStats;

interface StatUpdateProvider {
    id: number;
    stats: StatsFunc;
}

const defaultUpdater: StatsFunc = () => ({
    firepower: 0,
    antiAir: 0,
    fireRate: 0,
    damage: 0,
});

const _updaters = new Map<number, StatsFunc>();

/**
 * Marks a method as a weapon stat updater and automatically adds it to the
 * list of updaters here.
 */
export function weaponStatsUpdater() {
    /*
     * @NOTE: By using the target itself and not the property key we make it
     * not entirely a method decorator, we can put that decorator anywhere
     * in the class and it will still work. I need my typesafety!
     */
    return (target: StatUpdateProvider, _propertyKey: string, _descriptor: PropertyDescriptor) => {
        _updaters.set(target.id, target.stats);
    };
}

export const weaponUpdaters = {
    get: (id: number): StatsFunc => {
        return _updaters.get(id) || defaultUpdater;
    },
};
