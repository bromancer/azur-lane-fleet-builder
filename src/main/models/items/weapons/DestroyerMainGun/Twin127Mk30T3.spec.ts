import { ItemLevel, Equipment } from "../../Item";
import { Weapon, WeaponStat } from "../Weapon";
import Twin127Mk30T3 from "./Twin127Mk30T3";

describe("Twin 127 mm (5\"/38 Mk38) Type 3", () => {
    let gun: Weapon;

    beforeEach(() => {
        gun = Twin127Mk30T3.new();
    });

    it.each([
        ["firepower", 20],
        ["antiAir", 25],
    ])("Has the correct %s boost", (stat: WeaponStat, expected: number) => {
        expect(gun.stats[stat]).toBe(expected);
    });

    it.each([
        [0, 4],
        [1, 5],
        [2, 6],
        [3, 7],
        [4, 8],
        [5, 10],
        [6, 11],
        [7, 12],
        [8, 13],
        [9, 14],
        [10, 15],
    ])("Has the correct damage at level %d", (level: ItemLevel, expected: number) => {
        const g = Equipment.setLevel(gun, level);

        expect(g.stats.damage).toBe(expected);
    });
});
