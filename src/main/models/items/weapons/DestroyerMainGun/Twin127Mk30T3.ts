import { DestroyerMainGun } from "./DestroyerMainGun";
import { AmmoType } from "../AmmoType";
import { ItemType } from "../../ItemType";
import { ItemLevel, Equipment, ItemClassification } from "../../Item";
import { WeaponStats, weaponStatsUpdater } from "../Weapon";
import { SanDiegoVisuals } from "../../../../img/ships/SanDiego";

export default class Twin127Mk30T3 {
    private constructor() { }
    public static readonly id: number = 1;

    public static new(): DestroyerMainGun {
        return {
            classification: ItemClassification.Weapon,
            id: this.id,
            ammo: AmmoType.HighExplosive,
            coefficient: 1.1,
            volleyTime: 0.1,
            volley: 2,
            multiplier: 2,
            stats: {
                fireRate: 1.86,
                damage: 4,
                firepower: 20,
                antiAir: 25,
            },
            excluded: [],
            firingAngle: 360,
            spreadRange: 15,
            level: 0,
            range: 60,
            skill: undefined,
            type: ItemType.DestroyerGun,
            tier: "T0/3",
            visuals: SanDiegoVisuals.map.default,
            name: "Twin 127 Mk30",
        };
    }

    @weaponStatsUpdater()
    public static stats(level: ItemLevel): WeaponStats {
        return {
            fireRate: Equipment.autoValue(1.86, 1.53, level),
            damage: Equipment.autoValue(4, 15, level),
            firepower: 20,
            antiAir: 25,
        };
    }
}
