import { Weapon } from "../Weapon";
import { ItemType } from "../../ItemType";

export interface DestroyerMainGun extends Weapon {
    readonly type: ItemType.DestroyerGun;
    /**
     * Number of volleys fired
     */
    readonly volley: number;
    /**
     * Time between volleys
     */
    readonly volleyTime: number;
}
