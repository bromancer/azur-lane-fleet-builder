export enum AmmoType {
    HighExplosive,
    Normal,
    NormalPlus,
    ArmorPiercing,
}
