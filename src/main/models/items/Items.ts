import HellcatT3 from "./planes/HellcatT3";
import { Item } from "./Item";
import BarracudaT3 from "./planes/BarracudaT3";

export const items: Item[] = [];

items.push(HellcatT3.create());
items.push(BarracudaT3.create());
