import { Rarity } from "./Rarity";
import { HullType } from "./HullType";
import { ArmorType } from "./ArmorType";
import { Faction } from "../Faction";
import { ShipVisualMap } from "../../img/Img";
import { ItemSlot, Slot } from "../items/slots/ItemSlot";
import { Retrofit } from "./Retrofit";
import update from "immutability-helper";
import { shipUpdaters } from "./ShipGirlUpdate";
import { ShipStats } from "./ShipStats";
import { Item } from "../items/Item";

export type LimitBreak = 0 | 1 | 2 | 3;

export interface ShipGirl {
    readonly id: number;
    readonly shipClass: string;
    readonly faction: Faction;
    readonly armor: ArmorType;
    readonly hull: HullType;
    readonly itemSlots: ItemSlot[];
    readonly retrofits: Retrofit[];
    readonly level: number;
    readonly limitBreak: LimitBreak;
    readonly stats: ShipStats;
    readonly statsBoost: ShipStats;
    readonly visuals: ShipVisualMap;
    readonly name: string;
    readonly rarity: Rarity;
    readonly availableVisuals: string[];
    readonly selectedVisual: string;
}

export abstract class ShipGirl {
    public static setLimitBreak(ship: ShipGirl, limitBreak: LimitBreak): ShipGirl {
        if (limitBreak === ship.limitBreak) {
            // No need to update.
            return ship;
        }

        let newLevel = ship.level;

        if (limitBreak === 1 && ship.level < 10) {
            newLevel = 10;
        } else if (limitBreak === 2 && ship.level < 30) {
            newLevel = 30;
        } else if (limitBreak === 3 && ship.level < 70) {
            newLevel = 70;
        }

        return this.updateShip(ship, newLevel, limitBreak);
    }

    public static removeItem(ship: ShipGirl, slot: number): ShipGirl {
        if (ship.itemSlots.length < slot) {
            // ItemSlot index does not exist.
            return ship;
        }

        return update(ship, {
            itemSlots: {
                [slot]: {
                    item: {
                        $set: undefined,
                    },
                },
            },
        });
    }

    private static updateShip(ship: ShipGirl, level: number, limitBreak: LimitBreak): ShipGirl {
        return update(ship, {
            level: { $set: level },
            limitBreak: { $set: limitBreak },
            stats: { $set: ShipGirl.updateOwnStats(ship.id, level, limitBreak, ship.retrofits) },
            itemSlots: { $set: this.updateItemSlots(ship.itemSlots, limitBreak) },
            // @TODO: Activate skills?
        });
    }

    private static updateItemSlots(itemSlots: ItemSlot[], limitBreak: LimitBreak): ItemSlot[] {
        return itemSlots.map(slot => Slot.limitBreak(slot, limitBreak));
    }

    private static updateOwnStats(id: number, level: number, limitBreak: LimitBreak, retrofits: Retrofit[]): ShipStats {
        const updater = shipUpdaters.get(id);

        return updater(level, limitBreak, retrofits);
    }

    public static setLevel(ship: ShipGirl, level: number): ShipGirl {
        if (level === ship.level) {
            return ship;
        }

        let newLimitBreak = ship.limitBreak;

        if (level > 70 && ship.limitBreak < 1) {
            newLimitBreak = 1;
        }

        if (level > 80 && ship.limitBreak < 2) {
            newLimitBreak = 2;
        }

        if (level > 90 && ship.limitBreak < 3) {
            newLimitBreak = 3;
        }

        if (level < 70 && ship.limitBreak >= 3) {
            newLimitBreak = 2;
        }

        if (level < 30 && ship.limitBreak >= 2) {
            newLimitBreak = 1;
        }

        if (level < 10 && ship.limitBreak >= 1) {
            newLimitBreak = 0;
        }

        return this.updateShip(ship, level, newLimitBreak);
    }

    public static setVisuals(ship: ShipGirl, visual: string): ShipGirl {
        if (!ship.availableVisuals.includes(visual)) {
            return ship;
        }

        return update(ship, {
            selectedVisual: { $set: visual },
        });
    }

    public static equip(ship: ShipGirl, slot: number, item: Item): ShipGirl {
        const itemSlot = ship.itemSlots[slot];

        if (!itemSlot) {
            return ship;
        }

        if (!itemSlot.itemTypes.includes(item.type)) {
            return ship;
        }

        if (itemSlot.excluded.includes(item.id)) {
            return ship;
        }

        if (item.excluded.includes(ship.id)) {
            return ship;
        }

        const equippedSlot = Slot.equip(itemSlot, item);

        return update(ship, {
            itemSlots: {
                [slot]: { $set: equippedSlot },
            },
        });
    }
}
