export enum ArmorType {
    Light = "Light",
    Medium = "Medium",
    Heavy = "Heavy",
}
