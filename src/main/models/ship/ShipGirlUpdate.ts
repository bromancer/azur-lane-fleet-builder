import { AutoShipGirl, BaseStats } from "./AutoShipGirl";
import { LimitBreak } from "./ShipGirl";
import { Retrofit } from "./Retrofit";
import { ShipStats } from "./ShipStats";

type StatsFunc = (level: number, limitBreak: LimitBreak, retrofits: Retrofit[]) => ShipStats;

const defaultUpdater: StatsFunc = () => {
    // Return all 0
    return AutoShipGirl.ownStats(AutoShipGirl.createBaseStats({}), 1);
};

/**
 * List of all updaters mapped by ship id.
 */
const _updaters = new Map<number, StatsFunc>();

interface ShipDataProvider {
    id: number;
    baseStats: BaseStats;
}

/**
 * Decorator used on ship constructors
 *
 * @note This decorator doesn't do anything unless the class it is on is called
 * at least once. In our case that means that we need to call .new() on it.
 * This is automatically done in the Ships.ts file which creates a list of all ships.
 *
 * @param constructor Constructor
 */
export function ship(constructor: ShipDataProvider) {
    const func: StatsFunc = (level, _limitbreak, _retrofits) => {
        return AutoShipGirl.ownStats(constructor.baseStats, level);
    };

    _updaters.set(constructor.id, func);
}

export const shipUpdaters = {
    get: (id: number): StatsFunc => {
        return _updaters.get(id) || defaultUpdater;
    },
    set: (id: number, func: StatsFunc) => {
        _updaters.set(id, func);
    },
};
