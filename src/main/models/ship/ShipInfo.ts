import { SkillLevel } from "../skill/Skill";
import { ShipGirl } from "./ShipGirl";
import { HullType } from "./HullType";

export interface IFleetInfo {
    /**
     * Flagship is required
     */
    flagship: ShipGirl;
    /**
     * Other ships in the backline (excluding flagship)
     */
    main: ShipGirl[];
    /**
     * Vanguard ships
     */
    vanguard: ShipGirl[];
}

export interface SkillLevelInfo {
    [key: string]: SkillLevel;
}

export interface ShipInfo {
    readonly self: ShipGirl;
    readonly fleet: IFleetInfo;
    readonly skillLevel: SkillLevelInfo;
}

export const vanguardHullTypes = [
    HullType.Destroyer,
    HullType.HeavyCruiser,
    HullType.LightCruiser,
];

export const mainHullTypes = [
    HullType.AircraftCarrier,
    HullType.AviationBattleship,
    HullType.Battlecruiser,
    HullType.Battleship,
    HullType.LightAircraftCarrier,
    HullType.Monitor,
    HullType.RepairShip,
];
