import { ShipStat, ShipStats, isShipStat } from "./ShipStats";
import { Equipment } from "../items/Item";

interface BaseStat {
    base: number;
    base100: number;
    base120: number;
}

export type BaseStats = {
    [key in ShipStat]: BaseStat;
};

/**
 * Auto ship girl attempts to determine leveling curve depending on the base
 * stats of the wiki. This is naturally wildly inaccurate but it's the best
 * we can do for now.
 *
 * Unfortunately for us the "base 100" and "base 120" stats are including
 * limit break and enhance buffs. So we'll just ignore limit break value and
 * attempt to make some linear function with our 3 base points.
 *
 * @NOTE Stat increase seems to be fairly linear, but they do round the values.
 * It's unclear whether they are rounded, or rounded down.
 *
 * @NOTE LimitBreak buffs seem to be 1x 2x 3x the values of LB1 for LB1, LB2, LB3
 * respectively. There are some rounding issues. Curlew for exaple goes from a
 * +20 at LB1 to a +41 at LB2 to a +61 at LB3 boost for her Firepower.
 *
 * @NOTE Since I suspect most people will max out their ships I don't think
 * this is a big issue.
 */
export abstract class AutoShipGirl {
    private static createBaseStat(): BaseStat {
        return { base: 0, base100: 0, base120: 0 };
    }

    public static createBaseStats(data: Partial<BaseStats>): BaseStats {
        // Initialize all with 0 and then overwrite with set values,
        return {
            health: AutoShipGirl.createBaseStat(),
            reload: AutoShipGirl.createBaseStat(),
            speed: AutoShipGirl.createBaseStat(),
            firepower: AutoShipGirl.createBaseStat(),
            torpedo: AutoShipGirl.createBaseStat(),
            evasion: AutoShipGirl.createBaseStat(),
            luck: AutoShipGirl.createBaseStat(),
            antiAir: AutoShipGirl.createBaseStat(),
            aviation: AutoShipGirl.createBaseStat(),
            oilCost: AutoShipGirl.createBaseStat(),
            accuracy: AutoShipGirl.createBaseStat(),
            antiSubmarine: AutoShipGirl.createBaseStat(),
            ...data,
        };
    }

    private static ownStat(base: BaseStat, level: number): number {
        if (level < 100) {
            // Levels 1 through 99
            const min = base.base;
            const max = base.base100;

            // We can only take 99 steps from 1 to 100
            const step = (max - min) / 99;

            // At level 1 we should simply return base value.
            return Math.round(min + (step * (level - 1)));
        } else if (level === 100) {
            // We know this exact stat!
            return base.base100;
        } else if (level > 100 && level < 120) {
            // Levels 101 through 119
            const min = base.base100;
            const max = base.base120;

            // We can take 20 steps from 100 to 120!
            const step = (max - min) / 20;

            return Math.round(min + (step * (level - 100)));
        } else {
            // Any level that's 120 or larger.
            return base.base120;
        }
    }

    public static ownStats(baseStats: BaseStats, level: number): ShipStats {
        const v: any = {};

        for (const stat in baseStats) {
            if (!isShipStat(stat)) {
                continue;
            }

            v[stat] = AutoShipGirl.ownStat(baseStats[stat], level);
        }

        return v as ShipStats;
    }

    private static itemStatBoost(stat: ShipStat, items: Equipment[]) {
        return items.reduce((acc, item) => acc + (item.stats[stat] || 0), 0);
    }

    public static boostStats(items: Equipment[]): ShipStats {
        return {
            health: AutoShipGirl.itemStatBoost("health", items),
            reload: AutoShipGirl.itemStatBoost("reload", items),
            speed: AutoShipGirl.itemStatBoost("speed", items),
            firepower: AutoShipGirl.itemStatBoost("firepower", items),
            torpedo: AutoShipGirl.itemStatBoost("torpedo", items),
            evasion: AutoShipGirl.itemStatBoost("evasion", items),
            luck: AutoShipGirl.itemStatBoost("luck", items),
            antiAir: AutoShipGirl.itemStatBoost("antiAir", items),
            aviation: AutoShipGirl.itemStatBoost("aviation", items),
            oilCost: AutoShipGirl.itemStatBoost("oilCost", items),
            accuracy: AutoShipGirl.itemStatBoost("accuracy", items),
            antiSubmarine: AutoShipGirl.itemStatBoost("antiSubmarine", items),
        };
    }
}
