import { HullType } from "../../ship/HullType";
import { Rarity } from "../../ship/Rarity";
import { Retrofit } from "../Retrofit";
import { IllustriousVisuals } from "../../../img/ships/Illustrious";
import { Faction } from "../../Faction";
import { ArmorType } from "../../ship/ArmorType";
import { AutoShipGirl } from "../AutoShipGirl";
import { LimitBreak, ShipGirl } from "../ShipGirl";
import { ship } from "../ShipGirlUpdate";
import { ShipStats } from "../ShipStats";
import { FighterSlot } from "../../items/slots/FighterSlot";
import { TorpedoBomberSlot } from "../../items/slots/TorpedoBomberSlot";
import { PlaneLimitBreaks } from "../../items/slots/PlaneSlot";

@ship
export default class Illustrious {
    private constructor() { }
    public static baseStats = AutoShipGirl.createBaseStats({
        health: { base: 1123, base100: 5806, base120: 6561 },
        reload: { base: 43, base100: 102, base120: 117 },
        speed: { base: 30, base100: 30, base120: 30 },
        evasion: { base: 13, base100: 33, base120: 53 },
        luck: { base: 44, base100: 44, base120: 44 },
        antiAir: { base: 56, base100: 260, base120: 296 },
        aviation: { base: 76, base100: 358, base120: 400 },
        oilCost: { base: 4, base100: 13, base120: 13 },
        accuracy: { base: 31, base100: 81, base120: 81 },
    });

    public static readonly id: number = 145;

    public static new(): ShipGirl {
        const fighterLimitBreaks: PlaneLimitBreaks = new Map();

        fighterLimitBreaks.set(0, { count: 1, efficiency: 1.2 });
        fighterLimitBreaks.set(1, { count: 2, efficiency: 1.23 });
        fighterLimitBreaks.set(2, { count: 2, efficiency: 1.28 });
        fighterLimitBreaks.set(3, { count: 3, efficiency: 1.35 });

        const torpedoLimitBreaks: PlaneLimitBreaks = new Map();

        torpedoLimitBreaks.set(0, { count: 1, efficiency: 1.10 });
        torpedoLimitBreaks.set(1, { count: 1, efficiency: 1.10 });
        torpedoLimitBreaks.set(2, { count: 2, efficiency: 1.10 });
        torpedoLimitBreaks.set(3, { count: 2, efficiency: 1.10 });

        return {
            armor: ArmorType.Heavy,
            visuals: IllustriousVisuals.map,
            faction: Faction.RoyalNavy,
            id: Illustrious.id,
            name: "Illustrious",
            shipClass: "Illustrious",
            hull: HullType.AircraftCarrier,
            rarity: Rarity.SuperRare,
            itemSlots: [
                FighterSlot.new(fighterLimitBreaks),
                FighterSlot.new(fighterLimitBreaks),
                TorpedoBomberSlot.new(torpedoLimitBreaks),
            ],
            level: 1,
            limitBreak: 0,
            retrofits: [],
            stats: AutoShipGirl.ownStats(Illustrious.baseStats, 1),
            statsBoost: AutoShipGirl.boostStats([]),
            availableVisuals: IllustriousVisuals.options,
            selectedVisual: IllustriousVisuals.default,
        };
    }

    public static stats(level: number, _limitBreak: LimitBreak, _retrofits: Retrofit[]): ShipStats {
        const stats = AutoShipGirl.ownStats(Illustrious.baseStats, level);

        return stats;
    }
}
