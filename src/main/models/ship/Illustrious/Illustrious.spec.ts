import { Illustrious } from ".";
import { ShipStat } from "../ShipStats";
import faker = require("faker");
import { ShipGirl } from "../ShipGirl";

describe("Downes", () => {
    describe("Base Stats", () => {
        let ship: ShipGirl;

        beforeEach(() => {
            ship = Illustrious.new();
        });

        it.each([
            ["health", 1123],
            ["reload", 43],
            ["speed", 30],
            ["firepower", 0],
            ["torpedo", 0],
            ["evasion", 13],
            ["luck", 44],
            ["antiAir", 56],
            ["aviation", 76],
            ["oilCost", 4],
            ["accuracy", 31],
            ["antiSubmarine", 0],
        ])(
            "Has the correct %s, expected: %d",
            (stat: ShipStat, expected: number) => {
                expect(ship.stats[stat]).toBe(expected);
            },
        );
    });

    describe("Level 100 Stats", () => {
        let ship: ShipGirl;

        beforeEach(() => {
            const s = Illustrious.new();
            ship = ShipGirl.setLevel(s, 100);
        });

        it.each([
            ["health", 5806],
            ["reload", 102],
            ["speed", 30],
            ["firepower", 0],
            ["torpedo", 0],
            ["evasion", 33],
            ["luck", 44],
            ["antiAir", 260],
            ["aviation", 358],
            ["oilCost", 13],
            ["accuracy", 81],
            ["antiSubmarine", 0],
        ])(
            "Has the correct %s, expected: %d",
            (stat: ShipStat, expected: number) => {
                expect(ship.stats[stat]).toBe(expected);
            },
        );
    });

    describe("Level 120 Stats", () => {
        let ship: ShipGirl;

        beforeEach(() => {
            const s = Illustrious.new();
            ship = ShipGirl.setLevel(s, 120);
        });

        it.each([
            ["health", 6561],
            ["reload", 117],
            ["speed", 30],
            ["firepower", 0],
            ["torpedo", 0],
            ["evasion", 53],
            ["luck", 44],
            ["antiAir", 296],
            ["aviation", 400],
            ["oilCost", 13],
            ["accuracy", 81], // wiki has no info on Accuracy for level 120
            ["antiSubmarine", 0],
        ])(
            "Has the correct %s, expected: %d",
            (stat: ShipStat, expected: number) => {
                expect(ship.stats[stat]).toBe(expected);
            },
        );
    });

    describe("Stat Fuzz", () => {
        /*
         The idea is that the values are at least between base and 100/120.
         */
        describe("Level 2 - 99", () => {
            let ship: ShipGirl;

            beforeEach(() => {
                const s = Illustrious.new();
                ship = ShipGirl.setLevel(s, faker.random.number({ min: 2, max: 99 }));
            });

            it.each([
                ["health", 1123, 5806],
                ["reload", 43, 102],
                ["speed", 30, 30],
                ["firepower", 0, 0],
                ["torpedo", 0, 0],
                ["evasion", 13, 33],
                ["luck", 44, 44],
                ["antiAir", 56, 260],
                ["aviation", 76, 358],
                ["oilCost", 4, 13],
                ["accuracy", 31, 81],
                ["antiSubmarine", 0, 0],
            ])(
                "%s is at least between [%d, %d]",
                (stat: ShipStat, lower: number, upper: number) => {
                    const shipStat = ship.stats[stat];

                    expect(shipStat).toBeGreaterThanOrEqual(lower);
                    expect(shipStat).toBeLessThanOrEqual(upper);
                },
            );
        });

        describe("Level 101 - 119", () => {
            let ship: ShipGirl;

            beforeEach(() => {
                const s = Illustrious.new();
                ship = ShipGirl.setLevel(s, faker.random.number({ min: 101, max: 119 }));
            });

            it.each([
                ["health", 5806, 6561],
                ["reload", 102, 117],
                ["speed", 30, 30],
                ["firepower", 0, 0],
                ["torpedo", 0, 0],
                ["evasion", 33, 53],
                ["luck", 44, 44],
                ["antiAir", 260, 296],
                ["aviation", 358, 400],
                ["oilCost", 13, 13],
                ["accuracy", 81, 81],
                ["antiSubmarine", 0, 0],
            ])(
                "%s is at least between [%d, %d]",
                (stat: ShipStat, lower: number, upper: number) => {
                    const shipStat = ship.stats[stat];

                    expect(shipStat).toBeGreaterThanOrEqual(lower);
                    expect(shipStat).toBeLessThanOrEqual(upper);
                },
            );
        });
    });
});
