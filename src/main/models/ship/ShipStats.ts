export type ShipStat = keyof ShipStats;

export function isShipStat(x: string): x is ShipStat {
    switch (x) {
        case "health":
        case "firepower":
        case "reload":
        case "torpedo":
        case "evasion":
        case "antiAir":
        case "aviation":
        case "speed":
        case "antiSubmarine":
        case "oilCost":
        case "luck":
        case "accuracy":
            return true;
        default:
            return false;
    }
}

export interface ShipStats {
    readonly health: number;
    readonly firepower: number;
    readonly reload: number;
    readonly torpedo: number;
    readonly evasion: number;
    readonly antiAir: number;
    readonly aviation: number;
    readonly speed: number;
    readonly antiSubmarine: number;
    readonly oilCost: number;
    readonly luck: number;
    readonly accuracy: number;
}
