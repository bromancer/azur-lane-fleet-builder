import { BaseStats, AutoShipGirl } from "./AutoShipGirl";
import { ArmorType } from "./ArmorType";
import { HullType } from "./HullType";
import { Faction } from "../Faction";
import { Rarity } from "./Rarity";
import { ShipGirl } from "./ShipGirl";
import { shipUpdaters } from "./ShipGirlUpdate";
import { Illustrious } from "./Illustrious";
import { Downes } from "./Downes";

import shipsJson from "./ships.json";
import { shipVisualsMap } from "../../img/ships/ShipVisuals";
import { ItemSlot } from "../items/slots/ItemSlot";
import { isWeaponSlot } from "../items/slots/WeaponSlot";
import { isPlaneSlot } from "../items/slots/PlaneSlot";

interface ShipData {
    id: number;
    armor: ArmorType;
    hull: HullType;
    faction: Faction;
    name: string;
    shipClass: string;
    rarity: Rarity;
    stats: BaseStats;
    itemSlots: ItemSlot[];
}

const shipsData = shipsJson as ShipData[];

const s = shipsData
    // Filter out Illustrious and Downes for now
    .filter(ship => ship.id !== 145 && ship.id !== 6)
    .map<ShipGirl>(ship => {
        const { stats, itemSlots, ...props } = ship;

        shipUpdaters.set(props.id, (le, _li, _re) => AutoShipGirl.ownStats(stats, le));
        const visuals = shipVisualsMap.get(props.id);

        itemSlots.forEach(slot => {
            if (isPlaneSlot(slot) || isWeaponSlot(slot)) {
                // Overwrite limitBreaks with actual map.
                (slot as any).limitBreaks = new Map(slot.limitBreaks);
            }
        });

        return {
            ...props,
            visuals: visuals.map,
            itemSlots,
            level: 1,
            limitBreak: 0,
            retrofits: [],
            stats: AutoShipGirl.ownStats(stats, 1),
            statsBoost: AutoShipGirl.boostStats([]),
            availableVisuals: visuals.options,
            selectedVisual: visuals.default,
        };
    });

s.push(Illustrious.new());
s.push(Downes.new());

export const ships = s.sort((a, b) => a.name.localeCompare(b.name));
