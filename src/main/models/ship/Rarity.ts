/**
 * Ship Rarity, string values are used in CSS, do not change.
 */
export enum Rarity {
    Common = "common",
    Rare = "rare",
    Elite = "elite",
    SuperRare = "super_rare",
    UltraRare = "ultra_rare",
}
