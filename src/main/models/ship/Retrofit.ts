import { ShipStat } from "./ShipStats";
import { HullType } from "./HullType";
import { ISkill } from "../skill/Skill";
import { LimitBreak } from "./ShipGirl";

export interface Retrofit {
    /**
     * Will return the increase, if any, of the requested ship stat.
     * If the stat is not affected this will return 0.
     *
     * @param stat ShipStat to query.
     *
     * @returns Increase in stat, if any.
     */
    getStatIncrease: (stat: ShipStat) => number;
    /**
     * @returns True if the ship is modernized, false otherwise.
     */
    modernizes: boolean;
    /**
     * Gets whether or not this retrofit changes the ship's hull type.
     *
     * if this returns undefined the ship keeps their original hull type.
     *
     * @returns HullType on change, undefined if unchanged.
     */
    getHullChange: () => HullType | undefined;
    /**
     * Gets an skill that is unlocked, if any.
     *
     * @returns Skill if unlocked, undefined if no skill has been unlocked.
     */
    getSkill: () => ISkill | undefined;

    /**
     * Index, used in retrofitRequirement
     */
    index: string;
    /**
     * Other retrofits required for this to be selectable.
     */
    retrofitRequirement: string[];
    /**
     * Ship level required for this to be selectable.
     */
    levelRequirement: number;
    /**
     * Ship limit break required for this to be selectable.
     */
    limitBreakRequirement: LimitBreak;

    /**
     * Number of levels this retrofit has.
     */
    levels: number;
    /**
     * Current level of retrofit, 0 means disabled.
     */
    level: number;
}
