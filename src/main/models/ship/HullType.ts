export enum HullType {
    /**
     * Destroyer, DD
     */
    Destroyer = "Destroyer",
    /**
     * Cruiser Light, CL
     */
    LightCruiser = "Light Cruiser",
    /**
     * Cruiser Assault, CA
     */
    HeavyCruiser = "Heavy Cruiser",
    /**
     * Battleship, BB
     */
    Battleship = "Battleship",
    /**
     * Battlecruiser, BC
     */
    Battlecruiser = "Battlecruiser",
    /**
     * Monitor, BM
     */
    Monitor = "Monitor",
    /**
     * Avation Battleship, BBV
     */
    AviationBattleship = "Aviation Battleship",
    /**
     * Cruiser Velor Light, CVL
     */
    LightAircraftCarrier = "Light Aircraft Carrier",
    /**
     * Cruiser Velor, CV
     */
    AircraftCarrier = "Aircraft Carrier",
    /**
     * Repair Ship, AR
     */
    RepairShip = "Repair Ship",
    /**
     * Attack Submarine, SS
     */
    Submarine = "Submarine",
}
