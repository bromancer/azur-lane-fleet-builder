import { Downes } from ".";
import { ShipStat } from "../ShipStats";
import faker = require("faker");
import { ShipGirl } from "../ShipGirl";

describe("Illustrious", () => {
    describe("Base Stats", () => {
        let ship: ShipGirl;

        beforeEach(() => {
            ship = Downes.new();
        });

        it.each([
            ["health", 280],
            ["reload", 70],
            ["speed", 44],
            ["firepower", 14],
            ["torpedo", 53],
            ["evasion", 60],
            ["luck", 63],
            ["antiAir", 31],
            ["aviation", 0],
            ["oilCost", 1],
            ["accuracy", 67],
            ["antiSubmarine", 44],
        ])(
            "Has the correct %s, expected: %d",
            (stat: ShipStat, expected: number) => {
                expect(ship.stats[stat]).toBe(expected);
            },
        );
    });

    describe("Level 100 Stats", () => {
        let ship: ShipGirl;

        beforeEach(() => {
            const s = Downes.new();
            ship = ShipGirl.setLevel(s, 100);
        });

        it.each([
            ["health", 1446],
            ["reload", 165],
            ["speed", 44],
            ["firepower", 66],
            ["torpedo", 248],
            ["evasion", 156],
            ["luck", 63],
            ["antiAir", 146],
            ["aviation", 0],
            ["oilCost", 7],
            ["accuracy", 174],
            ["antiSubmarine", 162],
        ])(
            "Has the correct %s, expected: %d",
            (stat: ShipStat, expected: number) => {
                expect(ship.stats[stat]).toBe(expected);
            },
        );
    });

    describe("Level 120 Stats", () => {
        let ship: ShipGirl;

        beforeEach(() => {
            const s = Downes.new();
            ship = ShipGirl.setLevel(s, 120);
        });

        it.each([
            ["health", 1735],
            ["reload", 190],
            ["speed", 44],
            ["firepower", 76],
            ["torpedo", 283],
            ["evasion", 162],
            ["luck", 63],
            ["antiAir", 168],
            ["aviation", 0],
            ["oilCost", 7],
            ["accuracy", 174], // wiki has no info on Accuracy for level 120
            ["antiSubmarine", 181],
        ])(
            "Has the correct %s, expected: %d",
            (stat: ShipStat, expected: number) => {
                expect(ship.stats[stat]).toBe(expected);
            },
        );
    });

    describe("Stat Fuzz", () => {
        /*
         The idea is that the values are at least between base and 100/120.
         */
        describe("Level 2 - 99", () => {
            let ship: ShipGirl;

            beforeEach(() => {
                const s = Downes.new();
                ship = ShipGirl.setLevel(s, faker.random.number({ min: 2, max: 99 }));
            });

            it.each([
                ["health", 280, 1446],
                ["reload", 70, 165],
                ["speed", 44, 44],
                ["firepower", 14, 66],
                ["torpedo", 53, 248],
                ["evasion", 60, 156],
                ["luck", 63, 63],
                ["antiAir", 31, 146],
                ["aviation", 0, 0],
                ["oilCost", 1, 7],
                ["accuracy", 67, 174],
                ["antiSubmarine", 44, 162],
            ])(
                "%s is at least between [%d, %d]",
                (stat: ShipStat, lower: number, upper: number) => {
                    const shipStat = ship.stats[stat];

                    expect(shipStat).toBeGreaterThanOrEqual(lower);
                    expect(shipStat).toBeLessThanOrEqual(upper);
                },
            );
        });

        describe("Level 101 - 119", () => {
            let ship: ShipGirl;

            beforeEach(() => {
                const s = Downes.new();
                ship = ShipGirl.setLevel(s, faker.random.number({ min: 101, max: 119 }));
            });

            it.each([
                ["health", 1446, 1735],
                ["reload", 165, 190],
                ["speed", 44, 44],
                ["firepower", 66, 76],
                ["torpedo", 248, 283],
                ["evasion", 156, 162],
                ["luck", 63, 63],
                ["antiAir", 146, 168],
                ["aviation", 0, 0],
                ["oilCost", 7, 7],
                ["accuracy", 174, 174],
                ["antiSubmarine", 162, 181],
            ])(
                "%s is at least between [%d, %d]",
                (stat: ShipStat, lower: number, upper: number) => {
                    const shipStat = ship.stats[stat];

                    expect(shipStat).toBeGreaterThanOrEqual(lower);
                    expect(shipStat).toBeLessThanOrEqual(upper);
                },
            );
        });
    });
});
