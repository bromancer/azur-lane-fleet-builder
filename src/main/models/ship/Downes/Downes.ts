import { AutoShipGirl } from "../AutoShipGirl";
import { HullType } from "../HullType";
import { Rarity } from "../Rarity";
import { DownesVisuals } from "../../../img/ships/Downes";
import { ArmorType } from "../ArmorType";
import { Faction } from "../../Faction";
import { ShipGirl, LimitBreak } from "../ShipGirl";
import { Retrofit } from "../Retrofit";
import { ShipStats } from "../ShipStats";
import { ship } from "../ShipGirlUpdate";

@ship
export default class Downes {
    private constructor() { }
    public static baseStats = AutoShipGirl.createBaseStats({
        health: { base: 280, base100: 1446, base120: 1735 },
        reload: { base: 70, base100: 165, base120: 190 },
        speed: { base: 44, base100: 44, base120: 44 },
        firepower: { base: 14, base100: 66, base120: 76 },
        torpedo: { base: 53, base100: 248, base120: 283 },
        evasion: { base: 60, base100: 156, base120: 162 },
        luck: { base: 63, base100: 63, base120: 63 },
        antiAir: { base: 31, base100: 146, base120: 168 },
        oilCost: { base: 1, base100: 7, base120: 7 },
        accuracy: { base: 67, base100: 174, base120: 174 },
        antiSubmarine: { base: 44, base100: 162, base120: 181 },
    });

    public static readonly id: number = 6;

    public static new(): ShipGirl {
        return {
            armor: ArmorType.Light,
            visuals: DownesVisuals.map,
            faction: Faction.EagleUnion,
            id: Downes.id,
            name: "Downes",
            shipClass: "Mahan",
            hull: HullType.Destroyer,
            rarity: Rarity.Common,
            itemSlots: [],
            level: 1,
            limitBreak: 0,
            retrofits: [],
            stats: AutoShipGirl.ownStats(Downes.baseStats, 1),
            statsBoost: AutoShipGirl.boostStats([]),
            availableVisuals: DownesVisuals.options,
            selectedVisual: DownesVisuals.default,
        };
    }

    public static stats(level: number, _limitBreak: LimitBreak, _retrofits: Retrofit[]): ShipStats {
        const stats = AutoShipGirl.ownStats(Downes.baseStats, level);

        return stats;
    }
}
