import React = require("react");
import ReactDOM = require("react-dom");
import { createHashHistory } from "history";
import { App } from "./App";

const history = createHashHistory();

ReactDOM.render(
    <App history={history} />,
    document.getElementById("root"),
);
