import { ISkillVisuals } from "../../Img";
import QueensOrdersVisuals = require("./Skill_10300.png");

interface QueensOrdersVisuals {
    default: ISkillVisuals;
}

const queensOrdersVisuals: QueensOrdersVisuals = {
    default: {
        icon: { imageMode: "single", src: QueensOrdersVisuals, size: { width: 0, height: 0 } },
    },
};

export default queensOrdersVisuals;
