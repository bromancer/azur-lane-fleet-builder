import SanDiego = require("./SanDiego.png");
import SanDiegoKai = require("./SanDiegoKai.png");
import SanDiegoIcon = require("./SanDiegoIcon.png");
import SanDiegoKaiIcon = require("./SanDiegoKaiIcon.png");
import SandyClausIcon = require("./SandyClausIcon.png");
import SandyClaus = require("./SandyClaus.png");
import { createShipVisualInfo } from "../../Img";

const sanDiegoVisuals = createShipVisualInfo(
    {
        default: {
            icon: {
                imageMode: "single",
                src: SanDiegoIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: SanDiego,
                xOffset: 70,
                yOffset: 70,
                size: { width: 674, height: 1024 },
            },
            full: {
                imageMode: "single",
                src: SanDiego,
                size: { width: 116, height: 116 },
            },
        },
        retrofit: {
            icon: {
                imageMode: "single",
                src: SanDiegoKaiIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: SanDiegoKai,
                xOffset: 350,
                yOffset: 70,
                size: { width: 1026, height: 909 },
            },
            full: {
                imageMode: "single",
                src: SanDiegoKai,
                size: { width: 1026, height: 909 },
            },
        },
        sandyClaus: {
            icon: {
                imageMode: "single",
                src: SandyClausIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: SandyClaus,
                xOffset: 170,
                yOffset: 20,
                size: { width: 963, height: 1024 },
            },
            full: {
                imageMode: "single",
                src: SandyClaus,
                size: { width: 963, height: 1024 },
            },
        },
    },
    "default",
);

export default sanDiegoVisuals;
