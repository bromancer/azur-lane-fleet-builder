import Illustrious = require("./Illustrious.png");
import IllustriousParty = require("./IllustriousParty.png");
import IllustriousPledge = require("./IllustriousPledge.png");
import IllustriousIcon = require("./IllustriousIcon.png");
import IllustriousTea = require("./IllustriousTea_Party.png");
import { createShipVisualInfo } from "../../Img";

const illustriousVisuals = createShipVisualInfo(
    {
        default: {
            icon: {
                imageMode: "single",
                src: IllustriousIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: Illustrious,
                xOffset: 200,
                yOffset: -10,
                size: { width: 951, height: 895 },
            },
            full: {
                imageMode: "single",
                src: Illustrious,
                size: { width: 951, height: 895 },
            },
        },
        party: {
            icon: {
                imageMode: "single",
                src: IllustriousIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: IllustriousParty,
                xOffset: 485,
                yOffset: 0,
                size: { width: 987, height: 1026 },
            },
            full: {
                imageMode: "single",
                src: IllustriousTea,
                size: { width: 987, height: 1026 },
            },
        },
        pledge: {
            icon: {
                imageMode: "single",
                src: IllustriousIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: IllustriousPledge,
                xOffset: 285,
                yOffset: -20,
                size: { width: 1024, height: 951 },
            },
            full: {
                imageMode: "single",
                src: IllustriousPledge,
                size: { width: 1024, height: 951 },
            },
        },
        tea: {
            icon: {
                imageMode: "single",
                src: IllustriousIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: IllustriousTea,
                xOffset: 250,
                yOffset: -20,
                size: { width: 921, height: 1024 },
            },
            full: {
                imageMode: "single",
                src: IllustriousTea,
                size: { width: 921, height: 1024 },
            },
        },
    },
    "default",
);

export default illustriousVisuals;
