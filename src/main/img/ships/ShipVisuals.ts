import { ShipVisualInfo } from "../Img";
import { SanDiegoVisuals } from "./SanDiego";
import { IllustriousVisuals } from "./Illustrious";
import { DownesVisuals } from "./Downes";
import { VictoriousVisuals } from "./Victorious";

const _shipVisualsMap = new Map<number, ShipVisualInfo>();
// #1
const defaultVisuals = SanDiegoVisuals;

_shipVisualsMap.set(36, SanDiegoVisuals);
_shipVisualsMap.set(145, IllustriousVisuals);
_shipVisualsMap.set(6, DownesVisuals);
_shipVisualsMap.set(146, VictoriousVisuals);

export const shipVisualsMap = {
    get: (id: number) => {
        return _shipVisualsMap.get(id) || defaultVisuals;
    },
};
