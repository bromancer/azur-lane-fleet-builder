import { createShipVisualInfo } from "../../Img";
import VictoriousIcon = require("./VictoriousIcon.png");
import VictoriousCasualIcon = require("./VictoriousCasualIcon.png");
import Victorious = require("./Victorious.png");
import VictoriousCasual = require("./VictoriousCasual.png");

const victoriousVisuals = createShipVisualInfo(
    {
        default: {
            icon: {
                imageMode: "single",
                src: VictoriousIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: Victorious,
                xOffset: 520,
                yOffset: 280,
                size: { width: 1400, height: 1360 },
            },
            full: {
                imageMode: "single",
                src: Victorious,
                size: { width: 1400, height: 1360 },
            },
        },
        casual: {
            icon: {
                imageMode: "single",
                src: VictoriousCasualIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: VictoriousCasual,
                xOffset: 320,
                yOffset: 0,
                size: { width: 867, height: 1026 },
            },
            full: {
                imageMode: "single",
                src: VictoriousCasual,
                size: { width: 867, height: 1026 },
            },
        },
    },
    "default",
);

export default victoriousVisuals;
