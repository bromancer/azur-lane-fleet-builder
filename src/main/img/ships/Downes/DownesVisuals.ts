import Downes = require("./Downes.png");
import DownesKai = require("./DownesKai.png");
import DownesIcon = require("./DownesIcon.png");
import { createShipVisualInfo } from "../../Img";

const downesVisuals = createShipVisualInfo(
    {
        default: {
            icon: {
                imageMode: "single",
                src: DownesIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: Downes,
                xOffset: 190,
                yOffset: -20,
                size: { width: 778, height: 1024 },
            },
            full: {
                imageMode: "single",
                src: Downes,
                size: { width: 778, height: 1024 },
            },
        },
        retrofit: {
            // @TODO: Find retrofit icon.
            icon: {
                imageMode: "single",
                src: DownesIcon,
                size: { width: 116, height: 116 },
            },
            card: {
                imageMode: "offset",
                src: DownesKai,
                xOffset: 210,
                yOffset: -20,
                size: { width: 846, height: 1024 },
            },
            full: {
                imageMode: "single",
                src: DownesKai,
                size: { width: 846, height: 1024 },
            },
        },
    },
    "default",
);

export default downesVisuals;
