/**
 * Basic image spec, contains the link to the image.
 */
interface IImageSpec {
    /**
     * Image file
     */
    src: string;
    size: IImageSizeSpec;
}

interface IImageSizeSpec {
    width: number;
    height: number;
}

interface ISingleImageSpec extends IImageSpec {
    imageMode: "single";
}

/**
 * Sprite images should be used to save loading time, although I'm not
 * sure how much this will actually save if we use HTTP/2. It's an old
 * way of only having to download 1 image instead of 300.
 *
 * A sprite sheet contains 1 or more images that all have the same size
 * and are ordered in a grid like pattern.
 */
interface ISpriteImageSpec extends IImageSpec {
    imageMode: "sprite-sheet";
    /**
     * Widths of sprites on the sheet
     */
    spriteWidth: number;
    /**
     * Heights of sprites on the sheet
     */
    spriteHeight: number;
    /**
     * X position of sprite on the sheet (measured in cells not px)
     */
    x: number;
    /**
     * Y position of sprite on the sheet (measured in cells not px)
     */
    y: number;
}

/**
 * Offset images should be used when the image size is set, but the image
 * we have is larger than the area we can display it in. The offsets provide
 * a viewport.
 *
 * Example usage is the character cards
 */
interface IOffsetImageSpec extends IImageSpec {
    imageMode: "offset";
    /**
     * X Offset of image
     */
    xOffset: number;
    /**
     * Y Offset of image
     */
    yOffset: number;
}

export type ImageSpec = ISingleImageSpec | ISpriteImageSpec | IOffsetImageSpec;

export interface IShipGirlVisuals {
    /**
     * Image used when dispaying character icon only.
     */
    icon: ImageSpec;
    /**
     * Image used on character cards.
     */
    card: ImageSpec;
    /**
     * Image used when displaying full character.
     */
    full: ImageSpec;
}

export interface ISkillVisuals {
    icon: ImageSpec;
}

export interface IEquipmentVisuals {
    icon: ImageSpec;
}

export interface ShipVisualMap {
    [key: string]: IShipGirlVisuals;
}

export function createShipVisualMap<M extends ShipVisualMap>(map: M): M {
    return map;
}

export function createShipVisualInfo<M extends ShipVisualMap>(map: M, def: keyof M) {
    return {
        default: def,
        map: createShipVisualMap(map),
        options: Object.keys(map),
    };
}

export interface ShipVisualInfo<M extends ShipVisualMap = ShipVisualMap> {
    default: string;
    map: M;
    options: string[];
}
