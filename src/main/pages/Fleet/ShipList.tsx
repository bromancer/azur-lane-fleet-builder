import React = require("react");
import { HullType } from "../../models/ship/HullType";
import { ships } from "../../models/ship/Ships";
import { ShipGirl } from "../../models/ship/ShipGirl";
import ShipIcon from "../../components/ShipIcon";

interface Props {
    // Type of ships that can be selected
    hullTypes: HullType[];
    // Ships that can no longer be selected.
    excluded: number[];
    onSelected: (ship: ShipGirl) => void;
}

interface State {
    search: string;
}

export default class ShipList extends React.PureComponent<Props, State> {
    public constructor(props: Props) {
        super(props);

        this.state = {
            search: "",
        };
    }

    public static defaultProps = {
        hullTypes: [
            HullType.AircraftCarrier,
            HullType.AviationBattleship,
            HullType.Battlecruiser,
            HullType.Battleship,
            HullType.Destroyer,
            HullType.HeavyCruiser,
            HullType.LightAircraftCarrier,
            HullType.LightCruiser,
            HullType.Monitor,
            HullType.RepairShip,
            HullType.Submarine,
        ],
        excluded: [],
        onSelected: () => undefined,
    };

    private renderShipRow(ship: ShipGirl) {
        const weaponSlots = ship.itemSlots.slice(0, 3);

        console.log(ship.name);
        console.log(weaponSlots);

        return (
            <div
                className="ship-row"
                onClick={() => { this.props.onSelected(ship); }}
                // @TODO: Should be replaced with id, but collab ships cause conflicts
                key={`${ship.name}_${ship.id}`}
            >
                <ShipIcon
                    image={ship.visuals[ship.selectedVisual].icon}
                    rarity={ship.rarity}
                    level={ship.level}
                />
                <div className="ship-info">
                    <div className="ship-name">{ship.name}</div>
                    <div className="ship-class">{ship.shipClass}</div>
                    <div className="ship-hull">{ship.hull}</div>
                </div>
                <div className="ship-slots">
                    <div>Slots:</div>
                    {weaponSlots.map((slot, key) => (
                        <div className="ship-slot" key={key}>{slot.itemTypes[0]}</div>
                    ))}
                </div>
            </div>
        );
    }

    private static shipFilter(ship: ShipGirl, search: string): boolean {
        const name = ship.name.toLowerCase();
        const shipClass = ship.shipClass.toLowerCase();

        return name.includes(search) || shipClass.includes(search);
    }

    public render() {
        return (
            <div>
                <input
                    onChange={(event) => {
                        this.setState({ search: event.target.value.toLowerCase() });
                    }}
                />
                <div className="ship-list">
                    {ships
                        .filter(s => this.props.hullTypes.includes(s.hull))
                        .filter(s => !this.props.excluded.includes(s.id))
                        .filter(s => ShipList.shipFilter(s, this.state.search))
                        .map(s => this.renderShipRow(s))
                    }
                </div>
            </div>
        );
    }
}
