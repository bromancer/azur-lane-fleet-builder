import React = require("react");
import { ShipGirl } from "../../models/ship/ShipGirl";
import { Fleet, FleetPosition } from "../../store/reducers/FleetReducer";
import Modal = require("react-modal");
import ShipList from "./ShipList";
import { HullType } from "../../models/ship/HullType";
import { mainHullTypes, vanguardHullTypes } from "../../models/ship/ShipInfo";

import "./fleet-page";
import { ShipOverview } from "./ShipOverview";
import { ShipEdit } from "./ShipEdit";

export interface FleetPageDispatchProps {
    onRemoveShip: (ship: ShipGirl, position: FleetPosition) => void;
    onAddShip: (ship: ShipGirl, position: FleetPosition) => void;
    onEditShip: (ship: ShipGirl, position: FleetPosition) => void;
}

export interface FleetPageProps {
    fleet: Fleet;
    selectedShipIds: number[];
}

interface ShipSelection {
    mode: "select";
    position: FleetPosition;
}

interface NoSelection {
    mode: "none";
}

interface EditSelection {
    mode: "edit";
    ship: ShipGirl;
    position: FleetPosition;
}

type Selection = ShipSelection | EditSelection | NoSelection;

interface State {
    selection: Selection;
}

Modal.setAppElement("#root");

type Props = FleetPageDispatchProps & FleetPageProps;

export default class FleetPageComponent extends React.PureComponent<Props, State> {
    public static defaultProps = {
        fleet: {},
        onRemoveShip: () => undefined,
        onAddShip: () => undefined,
    };

    public constructor(props: Props) {
        super(props);

        this.state = {
            selection: { mode: "none" },
        };
    }

    private showOverview() {
        this.setState({ selection: { mode: "none" } });
    }

    private isMainPosition(position: FleetPosition): boolean {
        return position === "flagship" || position === "mainSecondary" || position === "mainTertiary";
    }

    private renderShipList(selection: ShipSelection) {
        let hullTypes: HullType[];

        if (this.isMainPosition(selection.position)) {
            hullTypes = mainHullTypes;
        } else {
            hullTypes = vanguardHullTypes;
        }

        return (
            <ShipList
                hullTypes={hullTypes}
                excluded={this.props.selectedShipIds}
                onSelected={(ship) => {
                    this.props.onAddShip(ship, selection.position);
                    this.showOverview();
                }}
            />
        );
    }

    private renderMain() {
        switch (this.state.selection.mode) {
            case "edit": {
                const position = this.state.selection.position;
                const ship = this.state.selection.ship;

                return (
                    <ShipEdit
                        ship={this.state.selection.ship}
                        onCancel={() => {
                            this.showOverview();
                        }}
                        onSave={(updatedShip) => {
                            this.props.onEditShip(updatedShip, position);
                            this.showOverview();
                        }}
                        onRemove={() => {
                            this.props.onRemoveShip(ship, position);
                            this.showOverview();
                        }}
                    />
                );
            }
            case "none":
            case "select":
            default:
                return (
                    <ShipOverview
                        fleet={this.props.fleet}
                        onEditShip={(ship, position) => {
                            this.setState({ selection: { mode: "edit", ship, position } });
                        }}
                        onAddShip={(position) => {
                            this.setState({ selection: { mode: "select", position } });
                        }}
                    />
                );
        }
    }

    public render() {
        return (
            <div className="fleet-page">
                {/* Main Row */}
                {this.renderMain()}
                <Modal
                    isOpen={this.state.selection.mode === "select"}
                    contentLabel="Select Ship"
                    onRequestClose={() => { this.showOverview(); }}
                    className="ship-select"
                >
                    {/* Only render stuff when we need it! */}
                    {this.state.selection.mode === "select" && this.renderShipList(this.state.selection)}
                </Modal>
            </div >
        );
    }
}
