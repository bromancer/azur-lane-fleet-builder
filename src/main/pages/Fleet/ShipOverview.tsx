import React from "react";
import ShipIcon, { EmptyShipIcon } from "../../components/ShipIcon";
import { Fleet, FleetPosition } from "../../store/reducers/FleetReducer";
import { ShipGirl } from "../../models/ship/ShipGirl";

interface Props {
    fleet: Fleet;
    onEditShip: (ship: ShipGirl, position: FleetPosition) => void;
    onAddShip: (position: FleetPosition) => void;
}

export class ShipOverview extends React.PureComponent<Props> {
    private renderShip(position: FleetPosition) {
        const ship = this.props.fleet[position];

        return ship
            ? (
                <ShipIcon
                    image={ship.visuals[ship.selectedVisual].icon}
                    rarity={ship.rarity}
                    level={ship.level}
                    onClick={() => { this.props.onEditShip(ship, position); }}
                />
            ) : (
                <EmptyShipIcon
                    onClick={() => {
                        this.props.onAddShip(position);
                    }}
                />
            );
    }

    public render() {
        return (
            <div className="ship-overview">
                <div className="ship-row-title">
                    Main
                </div>
                <div className="ship-row">
                    {this.renderShip("mainSecondary")}
                    {this.renderShip("flagship")}
                    {this.renderShip("mainTertiary")}
                </div>
                {/* Vanguard Row */}
                <div className="ship-row-title">
                    Vanguard
                </div>
                <div className="ship-row">
                    {this.renderShip("vanguardPrimary")}
                    {this.renderShip("vanguardSecondary")}
                    {this.renderShip("vanguardTertiary")}
                </div>
            </div>
        );
    }
}
