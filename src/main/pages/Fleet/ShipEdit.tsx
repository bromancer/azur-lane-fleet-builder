import React from "react";
import { ShipConfig } from "../../components/ShipConfig";
import { ShipGirl } from "../../models/ship/ShipGirl";

interface Props {
    ship: ShipGirl;
    onCancel: () => void;
    onSave: (ship: ShipGirl) => void;
    onRemove: () => void;
}

interface State {
    ship: ShipGirl;
}

export class ShipEdit extends React.PureComponent<Props, State> {
    public constructor(props: Props) {
        super(props);

        this.state = {
            ship: props.ship,
        };
    }

    public render() {
        return (
            <div>
                <nav>
                    <button onClick={this.props.onCancel}>
                        Cancel
                    </button>
                    <button onClick={() => { this.props.onSave(this.state.ship); }}>
                        Save
                    </button>
                    <button onClick={this.props.onRemove}>
                        Delete
                    </button>
                </nav>
                <ShipConfig
                    ship={this.props.ship}
                    onChange={(ship) => { this.setState({ ship }); }}
                />
            </div>
        );
    }
}
