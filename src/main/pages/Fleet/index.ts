import { MapStateToProps, MapDispatchToProps, connect } from "react-redux";
import { AppState } from "../../store";
import FleetPageComponent, { FleetPageProps, FleetPageDispatchProps } from "./FleetPage";
import { add, remove, update } from "../../store/actions/FleetActions";

const mapStateToProps: MapStateToProps<FleetPageProps, {}, AppState> = (state, _ownProps) => {
    const selectedShipIds: number[] = [];

    if (state.fleet.flagship) {
        selectedShipIds.push(state.fleet.flagship.id);
    }

    if (state.fleet.mainSecondary) {
        selectedShipIds.push(state.fleet.mainSecondary.id);
    }

    if (state.fleet.mainTertiary) {
        selectedShipIds.push(state.fleet.mainTertiary.id);
    }

    if (state.fleet.vanguardPrimary) {
        selectedShipIds.push(state.fleet.vanguardPrimary.id);
    }

    if (state.fleet.vanguardSecondary) {
        selectedShipIds.push(state.fleet.vanguardSecondary.id);
    }

    if (state.fleet.vanguardTertiary) {
        selectedShipIds.push(state.fleet.vanguardTertiary.id);
    }

    return {
        fleet: state.fleet,
        selectedShipIds,
    };
};

const mapDispatchToProps: MapDispatchToProps<FleetPageDispatchProps, {}> = (dispatch, _ownProps) => ({
    onAddShip: (ship, position) => {
        dispatch(add({ ship, position }));
    },
    onRemoveShip: (ship, position) => {
        dispatch(remove({ ship, position }));
    },
    onEditShip: (ship, position) => {
        console.dir(ship);
        dispatch(update({ ship, position }));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FleetPageComponent);
