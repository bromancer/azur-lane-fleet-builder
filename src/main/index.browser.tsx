import React = require("react");
import ReactDOM = require("react-dom");
import { createBrowserHistory } from "history";
import { App } from "./App";

const history = createBrowserHistory();

ReactDOM.render(
    <App history={history} />,
    document.getElementById("root"),
);
