import { ShipGirl } from "../../models/ship/ShipGirl";
import { FleetActions } from "../actions/FleetActions";
import update from "immutability-helper";

export type FleetPosition = keyof FleetState;

interface FleetState {
    readonly flagship?: ShipGirl;
    readonly mainSecondary?: ShipGirl;
    readonly mainTertiary?: ShipGirl;
    readonly vanguardPrimary?: ShipGirl;
    readonly vanguardSecondary?: ShipGirl;
    readonly vanguardTertiary?: ShipGirl;
}

export type Fleet = FleetState;

const initialState: FleetState = {};

export const fleetReducer = (state = initialState, action: FleetActions) => {
    switch (action.type) {
        case "@fleet/ADD": {
            const { payload } = action;

            return update(state, {
                [payload.position]: { $set: payload.ship },
            });
        }
        case "@fleet/UPDATE": {
            const { payload } = action;

            return update(state, {
                [payload.position]: { $set: payload.ship },
            });
        }
        case "@fleet/REMOVE": {
            const { payload } = action;

            return update(state, {
                [payload.position]: { $set: undefined },
            });
        }
        default:
            return state;
    }
};
