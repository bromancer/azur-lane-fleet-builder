import {
    combineReducers,
    createStore,
    applyMiddleware,
    compose,
    Reducer,
    AnyAction,
} from "redux";
import { History } from "history";
import {
    routerMiddleware,
    connectRouter,
} from "connected-react-router";
import { fleetReducer } from "./reducers/FleetReducer";

type ExtractType<T> = T extends Reducer<infer X, AnyAction> ? X : null;

function reducers(history: History) {
    return combineReducers({
        fleet: fleetReducer,
        router: connectRouter(history),
    });
}

export function createAppStore(history: History) {
    return createStore(
        reducers(history),
        compose(applyMiddleware(routerMiddleware(history))),
    );
}

export type AppState = ExtractType<ReturnType<typeof reducers>>;
export type AppStore = ReturnType<typeof createAppStore>;
