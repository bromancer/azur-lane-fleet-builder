import { createStandardAction, ActionType } from "typesafe-actions";
import { ShipGirl } from "../../models/ship/ShipGirl";
import { FleetPosition } from "../reducers/FleetReducer";

export const ADD = "@fleet/ADD";
export const REMOVE = "@fleet/REMOVE";
export const UPDATE = "@fleet/UPDATE";

interface Payload {
    ship: ShipGirl;
    position: FleetPosition;
}

export const add = createStandardAction(ADD)<Payload>();
export const update = createStandardAction(UPDATE)<Payload>();
export const remove = createStandardAction(REMOVE)<Payload>();

const actions = {
    add,
    update,
    remove,
};

export type FleetActions = ActionType<typeof actions>;
