import React = require("react");
import "./ship-girl-card";

import { Rarity } from "../../models/ship/Rarity";
import classnames from "classnames";
import { ImageSpec } from "../../img/Img";

interface Props {
    level: number;
    name: string;
    rarity: Rarity;
    image: ImageSpec;
}

export default class ShipGirlCard extends React.PureComponent<Props> {
    public render() {
        const imgOffset = {
            x: 0,
            y: 0,
        };

        const image = this.props.image;

        if (image.imageMode === "offset") {
            imgOffset.x = image.xOffset;
            imgOffset.y = image.yOffset;
        } else if (image.imageMode === "sprite-sheet") {
            imgOffset.x = image.x * image.spriteWidth;
            imgOffset.y = image.y * image.spriteHeight;
        }

        const rarity = {
            rare: this.props.rarity === Rarity.Rare,
            elite: this.props.rarity === Rarity.Elite,
            ssr: this.props.rarity === Rarity.SuperRare,
            common: this.props.rarity === Rarity.Common,
        };

        const strokeWidth = 4;

        return (
            <svg
                className="ship-card"
                width={300}
                height={420}
            >
                <defs>
                    <clipPath id="img-cut">
                        <rect x={15} y={15} width={270} height={390} />
                    </clipPath>
                </defs>
                {/* Background */}
                <image
                    xlinkHref={image.src}
                    clipPath={"url(#img-cut)"}
                    x={-imgOffset.x}
                    y={-imgOffset.y}
                    width={image.size.width}
                    height={image.size.height}
                />
                <polyline
                    className="ship-card-background"
                    points={"15 15 285 15 285 405 15 405 15 15"}
                />
                {/* Level Shade */}
                <polygon
                    className="ship-card-name-shading"
                    points={"16 16 284 16 284 56 16 56"}
                />
                {/* Level */}
                <text
                    className="ship-card-level"
                    x={55}
                    y={51}
                >
                    {`Lv.${this.props.level}`}
                </text>
                {/* Name Shade */}
                <polygon
                    className="ship-card-name-shading"
                    points={"16 355 284 355 284 315 16 315"}
                />
                {/* Name */}
                <text
                    className="ship-card-name"
                    textAnchor="middle"
                    x={150}
                    y={345}
                >
                    {this.props.name}
                </text>
                {/* Upper left triangle */}
                <polyline
                    className={classnames(
                        "ship-card-border",
                        "triangle",
                        rarity,
                    )}
                    points={"10 10 10 38 38 10"}
                />
                {/* Lower right triangle */}
                <polyline
                    className={classnames(
                        "ship-card-border",
                        "triangle",
                        rarity,
                    )}
                    points={"290 410 290 382 262 410"}
                />
                {/* Border */}
                <polygon
                    className={classnames(
                        "ship-card-border",
                        rarity,
                    )}
                    points={"15 45 15 405 255 405 285 375 285 15 45 15 15 45"}
                    fill="none"
                    strokeLinejoin="miter"
                    strokeWidth={strokeWidth}
                />
            </svg>
        );
    }
}
