import React = require("react");
import "./empty-ship-icon";

interface Props {
    onClick?: () => void;
}

export default class EmptyShipIcon extends React.PureComponent<Props> {
    public render() {
        return (
            <svg
                className="empty-ship-icon"
                width={120}
                height={120}
                onClick={this.props.onClick}
            >
                {/* Border */}
                <path
                    className="border"
                    d={"M0,0 0,100, 20,120 120,120 120,20 100,0z"}
                />
                <path
                    className="corner"
                    d={"M8,16 8,8 16,8"}
                />
                <path
                    className="corner"
                    d={"M112,104 112,112 104,112"}
                />
                <path
                    className="plus"
                    d={"M60,60 80,60 60,60 60,80 60,60 40,60 60,60 60,40"}
                />
            </svg>
        );
    }
}
