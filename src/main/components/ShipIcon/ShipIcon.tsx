import { ImageSpec } from "../../img/Img";
import React = require("react");
import classnames from "classnames";
import { Rarity } from "../../models/ship/Rarity";
import "./ship-icon";

interface Props {
    image: ImageSpec;
    rarity: Rarity;
    level: number;
    onClick: () => void;
}

export default class ShipIcon extends React.PureComponent<Props> {
    public static defaultProps = {
        onClick: () => undefined,
    };

    public render() {
        const rarity = {
            rare: this.props.rarity === Rarity.Rare,
            elite: this.props.rarity === Rarity.Elite,
            ssr: this.props.rarity === Rarity.SuperRare,
            common: this.props.rarity === Rarity.Common,
        };

        return (
            <svg
                className="ship-icon"
                width={120}
                height={120}
                onClick={this.props.onClick}
            >
                <defs>
                    <linearGradient
                        id="grad"
                        x1="100%"
                        y1="0%"
                        x2="0%"
                        y2="0%"
                    >
                        <stop offset="0%" className="grad-stop-1" />
                        <stop offset="50%" className="grad-stop-2" />
                        <stop offset="100%" className="grad-stop-3" />
                    </linearGradient>
                    {/* @TODO: Add Icon Clip Path in case icons are not 116x116? */}
                </defs>
                {/* Icon Border */}
                <polygon
                    className={classnames(
                        "ship-icon-border",
                        rarity,
                    )}
                    points={"0 0 120 0 120 120 0 120"}
                    fill="none"
                />
                {/* Ship Icon */}
                <image
                    xlinkHref={this.props.image.src}
                    x={2}
                    y={2}
                    width={this.props.image.size.width}
                    height={this.props.image.size.height}
                />
                {/* Level Shade */}
                <polygon
                    className="ship-icon-level-shading"
                    points={"20 4 116 4 116 30 20 30"}
                    fill={"url(#grad)"}
                />
                {/* Level */}
                <text
                    className="ship-icon-level"
                    x={114}
                    y={26}
                    textAnchor="end"
                >
                    {`LV.${this.props.level}`}
                </text>
            </svg>
        );
    }
}
