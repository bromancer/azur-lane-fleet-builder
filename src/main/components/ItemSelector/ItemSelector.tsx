import { ItemSlot } from "../../models/items/slots/ItemSlot";
import { Item } from "../../models/items/Item";
import React from "react";
import ShipIcon, { EmptyShipIcon } from "../ShipIcon";
import { Rarity } from "../../models/ship/Rarity";
import Modal = require("react-modal");
import ItemList from "./ItemList";

interface Props {
    itemSlots: ItemSlot[];
    onEditItem: (itemSlot: ItemSlot, item: Item) => void;
    onItemRemoved: (itemSlot: ItemSlot) => void;
}

interface NoSelection {
    mode: "none";
}

interface ItemSelection {
    mode: "select";
    itemSlot: ItemSlot;
}

type Selection = NoSelection | ItemSelection;

interface State {
    selection: Selection;
}

export class ItemSelector extends React.PureComponent<Props, State> {
    public static defaultProps = {
        itemSlots: [],
        onEditItem: () => undefined,
        onItemRemoved: () => undefined,
    };

    public constructor(props: Props) {
        super(props);

        this.state = {
            selection: { mode: "none" },
        };
    }

    private selectItem(itemSlot: ItemSlot) {
        this.setState({ selection: { mode: "select", itemSlot } });
    }

    private renderIcon(itemSlot: ItemSlot, key: number) {
        const item = itemSlot.item;

        if (item) {
            return (
                <ShipIcon
                    image={item.visuals.icon}
                    level={item.level}
                    rarity={Rarity.SuperRare}
                    onClick={() => { this.props.onItemRemoved(itemSlot); }}
                    key={key}
                />
            );
        } else {
            return (
                <EmptyShipIcon
                    key={key}
                    onClick={() => { this.selectItem(itemSlot); }}
                />
            );
        }
    }

    private closeModal() {
        this.setState({ selection: { mode: "none" } });
    }

    private renderItemList(itemSlot: ItemSlot) {
        return (
            <ItemList
                excluded={[]}
                types={itemSlot.itemTypes}
                onSelected={(item) => {
                    this.props.onEditItem(itemSlot, item);
                    this.closeModal();
                }}
            />
        );
    }

    public render() {
        return (
            <div style={{ display: "flex", flexDirection: "column" }}>
                {this.props.itemSlots.map((itemSlot, index) => this.renderIcon(itemSlot, index))}
                <Modal
                    isOpen={this.state.selection.mode === "select"}
                    contentLabel="Select Item"
                    onRequestClose={() => this.closeModal()}
                >
                    {this.state.selection.mode === "select" && this.renderItemList(this.state.selection.itemSlot)}
                </Modal>
            </div>
        );
    }
}
