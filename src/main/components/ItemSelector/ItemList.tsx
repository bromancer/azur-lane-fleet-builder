import { ItemType } from "../../models/items/ItemType";
import React from "react";
import { items } from "../../models/items/Items";
import ShipIcon from "../ShipIcon";
import { Rarity } from "../../models/ship/Rarity";
import { Item } from "../../models/items/Item";

interface Props {
    excluded: number[];
    types: ItemType[];
    onSelected: (item: Item) => void;
}

interface State {
    search: string;
}

export default class ItemList extends React.PureComponent<Props, State> {
    public constructor(props: Props) {
        super(props);

        this.state = {
            search: "",
        };
    }

    public static defaultProps = {
        excluded: [],
        types: [
            ItemType.AntiAirGun,
            ItemType.AntiSubmarine,
            ItemType.Auxiliary,
            ItemType.BattleshipGun,
            ItemType.DestroyerGun,
            ItemType.DiveBomberPlanes,
            ItemType.FighterPlanes,
            ItemType.HeavyCruiserGun,
            ItemType.LightCruiserGun,
            ItemType.SeaPlanes,
            ItemType.ShipTorpedos,
            ItemType.SubmarineTorpedos,
            ItemType.TorpedoPlanes,
        ],
        onSelected: () => undefined,
    };

    public render() {
        return (
            <div>
                <input
                    onChange={(event) => {
                        this.setState({ search: event.target.value });
                    }}
                />

                <div>
                    {items
                        .filter(i => this.props.types.includes(i.type))
                        .filter(i => !this.props.excluded.includes(i.id))
                        .filter(i => i.name.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase()))
                        .map(i => (
                            <div
                                onClick={() => { this.props.onSelected(i); }}
                                key={i.id}
                            >
                                <ShipIcon image={i.visuals.icon} rarity={Rarity.Common} level={1} />
                                <span>{i.name}</span>
                            </div>
                        ))
                    }
                </div>
            </div>
        );
    }
}
