import React = require("react");
import ShipGirlCard from "../ShipGirlCard";
import { ShipGirl } from "../../models/ship/ShipGirl";
import "./ship-config";
import { isShipStat } from "../../models/ship/ShipStats";
import { ItemSelector } from "../ItemSelector/ItemSelector";
import { ItemSlot } from "../../models/items/slots/ItemSlot";
import { Item } from "../../models/items/Item";
import FighterMath from "../../combat/FighterMath";
import { ArmorType } from "../../models/ship/ArmorType";

interface Props {
    ship: ShipGirl;
    onChange: (ship: ShipGirl) => void;
}

interface State {
    ship: ShipGirl;
}

export default class ShipConfig extends React.PureComponent<Props, State> {
    public static defaultProps = {
        onChange: () => undefined,
    };

    public constructor(props: Props) {
        super(props);

        this.state = {
            ship: props.ship,
        };
    }

    private updateShipItemEquip(itemSlot: ItemSlot, item: Item) {
        const slotIndex = this.state.ship.itemSlots
            .findIndex(v => v === itemSlot);

        const ship = ShipGirl.equip(this.state.ship, slotIndex, item);

        this.setState({ ship }, () => this.props.onChange(ship));
    }

    private updateShipLevel(level: number) {
        const ship = ShipGirl.setLevel(this.state.ship, level);

        this.setState({ ship }, () => this.props.onChange(ship));
    }

    private updateShipVisuals(selected: string) {
        const ship = ShipGirl.setVisuals(this.state.ship, selected);

        this.setState({ ship }, () => this.props.onChange(ship));
    }

    public render() {
        const ship = this.state.ship;
        const card = ship.visuals[ship.selectedVisual].card;

        return (
            <div className="ship-config">
                <div className="ship-config-card">
                    <ShipGirlCard
                        image={card}
                        level={ship.level}
                        name={ship.name}
                        rarity={ship.rarity}
                    />
                </div>
                <div className="ship-config-info">
                    <div className="leveling-buttons">
                        <button onClick={() => this.updateShipLevel(1)}>1</button>
                        <button onClick={() => this.updateShipLevel(100)}>100</button>
                    </div>
                    <div className="ship-config-info-container">
                        {Object.keys(ship.stats).map(key => {
                            if (!isShipStat(key)) {
                                return null;
                            }

                            return (
                                <div key={key} className="ship-config-info-row">
                                    <span className="ship-config-info-name">
                                        {key}
                                    </span>
                                    <span className="ship-config-info-value">
                                        {ship.stats[key]}
                                    </span>
                                </div>
                            );
                        })}
                    </div>
                    <div>
                        {ship.availableVisuals.map((v, i) => (
                            <button
                                key={i}
                                onClick={() => this.updateShipVisuals(v)}
                            >
                                {v}
                            </button>
                        ))}
                    </div>
                </div>
                <div className="ship-config-equipment">
                    <ItemSelector
                        itemSlots={ship.itemSlots}
                        onEditItem={(itemSlot, item) => {
                            this.updateShipItemEquip(itemSlot, item);
                        }}
                    />
                </div>
                <div>
                    <div>Anti Air DPS: {FighterMath.antiAirDps(ship).toFixed(2)}</div>
                    <div>Anti Air Burst: {FighterMath.antiAirBurst(ship).toFixed(2)}</div>
                    <div>Anti Ship DPS (Light): {FighterMath.antiShipDps(ship, ArmorType.Light).toFixed(2)}</div>
                    <div>Anti Ship DPS (Medium): {FighterMath.antiShipDps(ship, ArmorType.Medium).toFixed(2)}</div>
                    <div>Anti Ship DPS (Heavy): {FighterMath.antiShipDps(ship, ArmorType.Heavy).toFixed(2)}</div>
                </div>
            </div>
        );
    }
}
