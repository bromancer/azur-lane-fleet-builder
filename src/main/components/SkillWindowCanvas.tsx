import React = require("react");
import { Stage, Layer, Rect, Line } from "react-konva";
import Konva from "konva";

interface State {
    inspect?: {
        x: number;
    };
}

interface Props {
    windows: Array<{ start: number, duration: number }>;
    time: number;
}

const WIDTH = 1000;

export default class SkillWindowCanvas extends React.PureComponent<Props, State> {
    public constructor(props: Props) {
        super(props);

        this.state = {};
    }

    private renderLine(x: number) {
        return (
            <Line
                x={x}
                y={0}
                points={[
                    0, 0,
                    0, window.innerHeight,
                ]}
                stroke="#FF0000"
                strokeWidth={5}
            />
        );
    }

    private renderWindowBars() {
        const pxPerTime = WIDTH / this.props.time;

        return this.props.windows.map((w, i) => (
            <Rect
                key={i}
                x={w.start * pxPerTime}
                y={25}
                width={w.duration * pxPerTime}
                height={50}
                fill="#00FF00"
            />
        ));
    }

    public render() {
        return (
            <Stage
                style={{
                    width: "100%",
                }}
                width={WIDTH}
                height={100}
            >
                <Layer
                    visible={!!this.state.inspect}
                >
                    {this.state.inspect && this.renderLine(this.state.inspect.x)}
                </Layer>
                <Layer>
                    {this.renderWindowBars()}
                </Layer>
                <Layer>
                    <Rect
                        width={window.innerWidth}
                        height={window.innerHeight}
                        onMouseMove={evt => {
                            const stage = evt.target.getStage() as Konva.Stage;
                            const pos = stage.getPointerPosition();
                            this.setState({ inspect: { x: pos.x } });
                        }}
                        fill="#11000011"
                        onMouseOut={_evt => {
                            console.log("Mouse Out");
                            this.setState({ inspect: undefined });
                        }}
                    />
                    <Rect />
                </Layer>
            </Stage >
        );
    }
}
