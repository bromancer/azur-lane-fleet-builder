import React = require("react");
import "./numeric-input";

interface Props {
    initial: number;
    min: number;
    max: number;
    onChange: (value: number) => void;
}

interface State {
    value: number;
}

export default class NumericInput extends React.PureComponent<Props, State> {
    public static defaultProps = {
        initial: 0,
        min: 0,
        max: 10,
        onChange: () => undefined,
    };

    public constructor(props: Props) {
        super(props);

        this.state = {
            value: props.initial,
        };
    }

    private updateValue(value: number) {
        if (value >= this.props.min && value <= this.props.max) {
            this.setState({ value }, () => this.props.onChange(value));
        }
    }

    public render() {
        return (
            <svg
                className="numeric-input"
                width={64}
                height={64}
            >
                {/* Background */}
                <rect
                    className="numeric-input-background"
                    x={5}
                    y={5}
                    width={54}
                    height={54}
                />
                {/* Upper Button */}
                <path
                    className="numeric-input-button upper"
                    d="M2,2 2,22 22,2z"
                    onMouseDown={(event) => {
                        event.preventDefault();
                        this.updateValue(this.state.value + 1);
                    }}
                />
                {/* Lower Button */}
                <path
                    className="numeric-input-button lower"
                    d="M62,62 42,62 62,42z"
                    onMouseDown={(event) => {
                        event.preventDefault();
                        this.updateValue(this.state.value - 1);
                    }}
                />
                {/* Border */}
                <path
                    className="numeric-input-border"
                    d="M6,26 6,58 37,58 58,37 58,6 26,6z"
                />
                <text
                    className="numeric-input-text"
                    textAnchor="middle"
                    x={32}
                    y={40}
                >
                    {this.state.value}
                </text>
            </svg>
        );
    }
}
