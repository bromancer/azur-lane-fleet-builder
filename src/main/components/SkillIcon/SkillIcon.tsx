import React = require("react");
import "./style";
import { ImageSpec } from "../../img/Img";

/*
 Skill Icons on the wiki are 128x128px, with a round edge of around
 8px. If we want to make it look like the new icon style we need to
 cut off this round edge and we have room for 112x112px
 */

interface Props {
    icon: ImageSpec;
}

export default class SkillIcon extends React.PureComponent<Props> {
    public render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="skill-icon"
                width={120}
                height={120}
            >
                <defs>
                    <clipPath id="icon-cut">
                        <polygon
                            points={"0 0 0 100 20 120 120 120 120 20 100 0"}
                        />
                    </clipPath>
                    <filter id="shadow">
                        <feGaussianBlur stdDeviation="1 1" result="shadow" />
                        <feOffset dx="1" dy="1" />
                    </filter>
                </defs>
                {/* Icon */}
                <image
                    xlinkHref={this.props.icon.src}
                    clipPath={"url(#icon-cut)"}
                    x={0}
                    y={0}
                    width={this.props.icon.size.width}
                    height={this.props.icon.size.height}
                />
                {/* Icon border */}
                <path
                    className="skill-icon-border"
                    d={"M0,0 0,100 20,120 120,120 120,20 100,0z"}
                    fill="none"
                />
                {/* Skill Title */}
                <text
                    className="skill-icon-title shadow"
                    x={1}
                    y={9}
                    textAnchor="start"
                >
                    SKILL
                </text>
                <text
                    className="skill-icon-title"
                    x={1}
                    y={9}
                    textAnchor="start"
                >
                    SKILL
                </text>
            </svg>
        );
    }
}
