import path = require("path");
import CleanWebpackPlugin = require("clean-webpack-plugin");
import HTMLWebpackPlugin = require("html-webpack-plugin");
import { Configuration } from "webpack";

const config: Configuration = {
  entry: {
    main: path.join(
      __dirname,
      "src",
      "main",
      /*
       We need to support hash router in case we deploy on some location (such as gitlab pages) which does not
       automatically redirect to index.html
       */
      process.env.router === "hash" ? "index.hash.tsx" : "index.browser.tsx"),
  },
  output: {
    path: path.join(__dirname, "public"),
    filename: "index.js",
  },
  resolve: {
    extensions: [".js", ".ts", ".tsx", ".scss", ".json", "!.spec.ts", "!.test.ts", "!.spec.tsx", "!.spec.tsx"],
  },
  watchOptions: {
    ignored: /\.awcache/,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, "src", "main"),
        loader: "awesome-typescript-loader",
        options: {
          useCache: true,
        },
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[hash].[ext]",
              context: path.resolve(__dirname, "src/"),
              useRelativePaths: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(["public"]),
    new HTMLWebpackPlugin({
      title: "Azur Lane Fleet Planner",
      description: "Azur Lane Fleet Planner",
      template: "index.html",
      inject: false,
    }),
  ],
};

export default config;
