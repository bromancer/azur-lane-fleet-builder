# Troubleshooting
This file contains some common problems you may encounter while using or while developing.

## Usage
None!

## Development
Web Development is hard.

### Images
Webpack automatically handles images for us, however the TypeScript compiler does not want to include an image file because it's not a source file. In order to solve this you need to trick the compiler into thinking it's just a normal file.
Lucky for us it's super easy to trick it, simply add the following line to `index.d.ts` in the `src/typings` directory.
```
declare module "*.ext" {
    const value: string;
    export = value;
}
```
Replace `ext` with the extension of the file you want to load.

When adding a new extension you also need to ignore it in jest, else it will attempt to load it as well which is not what you want.
There is a line in `package.json` name `moduleNameMapper` which points extensions to a mock file. Simply add the extension to the correct one.
