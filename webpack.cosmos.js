const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HTMLWebpackPlugin = require("html-webpack-plugin");

const config = {
    mode: "development",
    devtool: "inline-source-map",
    resolve: {
        extensions: [".js", ".ts", ".tsx", ".scss", ".json"],
    },
    watchOptions: {
        ignored: [
            ".awcache",
            "node_modules",
            ".git"
        ]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "awesome-typescript-loader",
                options: {
                    useCache: true,
                },
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[path][name].[ext]",
                            context: path.resolve(__dirname, "src"),
                            useRelativePaths: true,
                        },
                    },
                ],
            },
            {
                test: /\.s?css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(["dist"]),
        new HTMLWebpackPlugin({
            title: "Azur Lane Fleet Planner",
            description: "Azur Lane Fleet Planner",
            template: "index.html",
            inject: false,
        }),
    ],
};

module.exports = config;
